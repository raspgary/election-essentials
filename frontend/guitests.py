from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import unittest

PATH = "./chromedriver.exe"
driver = webdriver.Chrome(PATH)
url = "https://dev.thepolitician.me/"


class TestText(unittest.TestCase):

    # test for text on landing page
    def test_1(self):
        title = driver.find_element_by_id("slogan1")
        slogan = driver.find_element_by_id("slogan2")
        click_here = driver.find_element_by_id("clickBelowLabel")
        self.assertEqual(title.text, "The Politician")
        self.assertEqual(slogan.text, "Your One Stop Shop to Politics")
        self.assertEqual(
            click_here.text, "Click below to learn about elections near you!")

    # test card on landing page to go to politicians page
    def test_2(self):
        link_cards = driver.find_elements_by_class_name("picCard")
        correct_url = url+"politician/"
        card = link_cards[0]
        card.click()
        element_present = EC.presence_of_element_located(
            (By.ID, 'politicianPage'))
        WebDriverWait(driver, 10).until(element_present)
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)
        driver.back()

    # test navbar to go to about page
    def test_3(self):
        nav = driver.find_element_by_id("aboutNav")
        correct_url = url+"about/"
        nav.click()
        element_present = EC.presence_of_element_located(
            (By.ID, 'aboutPage'))
        WebDriverWait(driver, 10).until(element_present)
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)

    # tests about text
    def test_4(self):
        mission = driver.find_element_by_id("missionHeader")
        self.assertEqual(mission.text, "Our Mission")

    # tests about profile cards
    def test_5(self):
        missions = driver.find_elements_by_id("aboutName")
        sanjay = missions[0]
        self.assertEqual(sanjay.text, "Sanjay Yepuri")

    # tests about tools cards
    def test_6(self):
        link_cards = driver.find_elements_by_class_name("picCard")
        correct_url = "https://reactjs.org/docs/getting-started.html"
        card = link_cards[2]
        card.click()
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)
        driver.back()

    # tests about datasources cards
    def test_7(self):
        link_cards = driver.find_elements_by_class_name("picCard")
        correct_url = "https://developers.google.com/civic-information"
        card = link_cards[10]
        card.click()
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)
        driver.back()

    # test navbar to go to elections page
    def test_8(self):
        nav = driver.find_element_by_id("electionNav")
        correct_url = url+"election/"
        nav.click()
        element_present = EC.presence_of_element_located(
            (By.ID, 'electionsPage'))
        WebDriverWait(driver, 10).until(element_present)
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)

    # test navbar to go to policies page
    def test_9(self):
        nav = driver.find_element_by_id("policyNav")
        correct_url = url+"policy/"
        nav.click()
        element_present = EC.presence_of_element_located(
            (By.ID, 'policiesPage'))
        WebDriverWait(driver, 10).until(element_present)
        current_url = driver.current_url
        self.assertEqual(correct_url, current_url)


if __name__ == '__main__':
    driver.get(url)
    try:
        unittest.main()
    finally:
        driver.quit()
