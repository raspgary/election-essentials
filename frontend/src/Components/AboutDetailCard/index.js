import React from "react";
import "./AboutDetailCard.css";
import { Card } from "react-bootstrap";

// card for displaying developer profiles
function AboutDetailCard(props) {
  const { image, name, role, bio, commits, issues, unittests, linkedin } 
                                                              = props.data;

  // displays person's profile picture, name, role, bio, and Gitlab info
  return (
    <Card className="contributorCard">
      <Card.Img className="propic" variant="top" src={image} />
      <Card.Body>
        <a id="linkedinlink" href={linkedin}>{name}</a>
        <Card.Text>
          {role} <br />
          {bio} <br />
          Commits: {commits} <br />
          Issues: {issues} <br />
          Unit Tests: {unittests} <br />
        </Card.Text>
      </Card.Body>
    </Card>
  );
}
export default AboutDetailCard;
