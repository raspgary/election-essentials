import React, { useEffect } from "react";
import "./TwitterFeed.css";
import { Container, Row } from "react-bootstrap";

// gets the Twitter feed of a politician with Twitter Publish
function TwitterFeed(props) {
  let { twitterHandle } = props;
  const url = "https://twitter.com/" + twitterHandle + "?ref_src=twsrc%5Etfw";

  // add twitter API script to the twitter feed component
  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://platform.twitter.com/widgets.js";
    document.getElementsByClassName("twitterFeed")[0].appendChild(script);
  }, []);

  // uses the Twitter API to get the user's twitter feed
  return (
    <Container>
      <Row className="twitterFeed">
        <a className="twitter-timeline" href={url}>
          Tweets by {twitterHandle}
        </a>
      </Row>
    </Container>
  );
}

export { TwitterFeed };
