import React from "react";
import { Container, Row, Spinner } from "react-bootstrap";
import "./Loading.css";

// A spinner for loading page
function Loading(props) {
  return (
    <Container>
      <Row id="spinnerRow">
        <Spinner
          className="spinner"
          animation="border"
          role="status"
          variant="info"
        >
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Row>
    </Container>
  );
}

export default Loading;
export { Loading };
