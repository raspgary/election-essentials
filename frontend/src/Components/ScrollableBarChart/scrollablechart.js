import * as d3 from "d3";

// https://observablehq.com/@will-r-chase/scrollable-bar-chart

function roundedRect(x, y, width, height, r) {
  const arc = (r, sign) =>
    r
      ? `a${r * sign[0]},${r * sign[1]} 0 0 1 ${r * sign[2]},${r * sign[3]}`
      : "";
  r = [
    Math.min(r[0], height, width),
    Math.min(r[1], height, width),
    Math.min(r[2], height, width),
    Math.min(r[3], height, width),
  ];

  return `M${x + r[0]},${y}h${width - r[0] - r[1]}${arc(r[1], [1, 1, 1, 1])}v${
    height - r[1] - r[2]
  }${arc(r[2], [1, 1, -1, 1])}h${-width + r[2] + r[3]}${arc(r[3], [
    1,
    1,
    -1,
    -1,
  ])}v${-height + r[3] + r[0]}${arc(r[0], [1, 1, 1, -1])}z`;
}
function chart(marginLeft) {
  return (svg, data, { width, height }) => {
    const margin = { top: 100, right: 20, bottom: 70, left: marginLeft};

    const barwidth = 25;
    const corner = Math.floor(barwidth / 2 + 5);

    height = data.length * 27 + margin.top;

    const scaleY = d3
      .scaleOrdinal()
      .domain(data.map((d) => d.x))
      .range(d3.range(28, (data.length + 1) * 28, 28));

    const scaleX = d3
      .scaleLinear()
      .domain([0, d3.max(data.map((d) => d.y))])
      .range([margin.left, width - margin.right]);

    function make_x_gridlines() {
      return d3.axisBottom(scaleX).ticks(3);
    }

    const xAxis = (g) =>
      g
        .attr("transform", `translate(0, ${margin.top})`)
        .call(d3.axisTop(scaleX).tickSizeOuter(0).ticks(3))
        .call((g) => g.select(".domain").remove());

    const yAxis = (g) =>
      g
        .attr("transform", `translate(${margin.left}, ${margin.top - 15})`)
        .call(d3.axisLeft(scaleY).tickSizeOuter(0))
        .call((g) => g.select(".domain").remove());
    svg.attr("width", width).attr("height", height);

    svg
      .append("g")
      .selectAll("path")
      .data(data)
      .join("path")
      .attr("fill", "#bfd4db")
      .attr("d", (d, i) =>
        roundedRect(scaleX(0), i * 28 + margin.top, 1, barwidth, [
          0,
          0,
          corner,
          0,
        ])
      )
      .transition()
      .duration(1000)
      .attr("d", (d, i) =>
        roundedRect(
          scaleX(0),
          i * 28 + margin.top,
          scaleX(d.y) - scaleX(0),
          barwidth,
          [0, 0, corner, 0]
        )
      );

    svg.append("g").call(xAxis).style("font-size", "14px");
    //y axis
    svg.append("g").call(yAxis).style("font-size", "14px");

    svg
      .append("g")
      .attr("class", "grid")
      .attr("transform", "translate(0," + (height - margin.bottom) + ")")
      .attr("stroke-opacity", 0.1)
      .call(
        make_x_gridlines()
          .tickSize(-height + margin.top + margin.bottom)
          .tickFormat("")
      );

    svg
      .append("line")
      .attr("x1", margin.left)
      .attr("x2", margin.left)
      .attr("y1", margin.top - 6)
      .attr("y2", height)
      .attr("stroke-width", "2px")
      .style("stroke", "black")
      .style("opacity", 1);
  }
}
export default chart;
