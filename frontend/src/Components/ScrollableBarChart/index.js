import React from "react";
import D3Wrapper from "../D3Wrapper";
import Loading from "../Loading";
import barchart from "./scrollablechart";

import "./barchart.css";

function ScrollableBarChart(props) {
  const { data, width } = props;

  return data ? (
    <div className="barchart">
      <D3Wrapper
        chart={barchart(width)}
        height={400}
        width={800}
        data={data.slice(0, Math.min(data.length, 50))}
      />{" "}
    </div>
  ) : (
    <Loading />
  );
}

export default ScrollableBarChart;
