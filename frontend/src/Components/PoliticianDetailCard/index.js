import React from "react";
import malePlaceholderImg from "../../Images/politician.png";
import femalePlaceholderImg from "../../Images/fem_politician.png";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDemocrat, faRepublican } from "@fortawesome/free-solid-svg-icons";

import "./PoliticianDetailCard.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 240,
  },
});

// a card to display a politician and some quick info
function PoliticianDetailCard(props) {
  const { data, highlight } = props;

  const classes = useStyles();
  const primary_key = data.id;

  // some quick info
  const basicFields = [
    { name: "Party", key: "party" },
    { name: "District", key: "district" },
  ];

  // set border color by party
  const party =
    data.party === "DEM" ? (
      <FontAwesomeIcon className="dem" icon={faDemocrat} />
    ) : data.party === "REP" ? (
      <FontAwesomeIcon className="rep" icon={faRepublican} />
    ) : (
      <></>
    );

  // set default image if no image
  var photo = data.photo;
  if (photo === "") {
    photo = data.gender === "F" ? femalePlaceholderImg : malePlaceholderImg;
  }

  // display the politicians image, name, party, and state
  return (
    <Card className={classes.root}>
      <Link
        component={CardActionArea}
        color="primary"
        size="small"
        to={"/politician/" + primary_key}
      >
        <CardMedia
          className={classes.media}
          image={photo}
          id="portraitImg"
          onError={(e) => {
            e.target.onerror = null;
            e.target.src =
              data.gender === "F" ? femalePlaceholderImg : malePlaceholderImg;
          }}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            <div className="card-title">
              {highlight ? (
                <Highlight attribute="name" hit={data} />
              ) : (
                data.name
              )}{" "}
              {party}
            </div>
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <ListGroup>
              {basicFields.map((f, i) => {
                return (
                  <ListGroupItem key={i}>
                    {highlight ? (
                      <Highlight attribute={f.key} hit={data} />
                    ) : (
                      data[f.key]
                    )}
                  </ListGroupItem>
                );
              })}
              {data.election ? (
                <ListGroupItem key="district">
                  {highlight ? (
                    <Highlight attribute={"election.branch"} hit={data} />
                  ) : (
                    data.election.branch
                  )}
                </ListGroupItem>
              ) : (
                <></>
              )}
            </ListGroup>
          </Typography>
        </CardContent>
      </Link>
    </Card>
  );
}
export { PoliticianDetailCard };
