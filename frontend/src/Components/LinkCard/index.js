import "./LinkCard.css";
import React from "react";
import { Card } from "react-bootstrap";

// a card component that goes to a tied link
function LinkDetailCard(props) {
  const { image, link, name, description } = props.data || props;

  // redirect to utl
  function goToURL(url) {
    window.location.href = url;
  }

  // dispalys an image, a title, and a description
  return (
    <Card onClick={() => goToURL(link)} className="picCard">
      {image ? (
        <Card.Img className="resourcePic" variant="top" src={image} />
      ) : (
        <> </>
      )}
      <Card.Body>
        <Card.Title className="resourceTitle">{name}</Card.Title>
        <Card.Text>{description}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default LinkDetailCard;
