import React from "react";

import { ListGroup } from "react-bootstrap";


// helper to format currency
function currencyFormat(num) {
  return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// a field to render details
function Field(props) {
  let { link, currency, name, value } = props;

  if (currency) {
    value = currencyFormat(value);
  } else if (link) {
    value = <a style={{"text-decoration": "underline"}} href={value}>link</a>
  }

  return (
    <ListGroup.Item>
      {" "}
      <str>{name}:</str> {value}
    </ListGroup.Item>
  );
}

export default Field;