import React, { useState, useEffect } from "react";
import { DetailView, makeFields } from "../Detail";
import { Container, Row } from "react-bootstrap";
import GridView from "../GridView";
import DistrictMap from "../DistrictMap";
import PieChart from "../PieChart";
import { PoliticianDetailCard } from "../PoliticianDetailCard";
import { getPoliticians } from "../../Library/Api";
import "./ElectionDetail.css";

// shows all the details about an election instance
function ElectionDetail(props) {
  const { election } = props;
  const [politicians, setPoliticians] = useState(undefined);

  // a header for the election info
  const header = (
    <Container>
      <h2>
        {" "}
        {election.branch} Election in {election.state}{" "}
      </h2>
    </Container>
  );

  // find who is the incumbent for the election
  function setIncumbent(politicians) {
    election.incumbent = "None";
    for (var i in politicians) {
      var politician = politicians[i];
      if (politician.incumbent) {
        election.incumbent = politician.name;
      }
    }
  }
  setIncumbent(politicians || []);

  // get the poll data for the pie chart
  function setPoll(politicians) {
    var pollObject = {};
    var leftover = 100;
    for (let i in politicians) {
      let politician = politicians[i];
      if (politician.poll > 0) {
        pollObject[politician.name] = politician.poll;
        leftover -= politician.poll;
      }
    }

    // don't make a pollObject to display if no poll data for election
    if (leftover === 100) {
      pollObject = undefined;
    }
    // set leftover to other
    else if (leftover > 0) {
      pollObject["other"] = leftover;
    }

    return pollObject;
  }
  const pollData = setPoll(politicians || []);

  // get the politicians for the election
  useEffect(() => {
    getPoliticians({ election_id: election.id })
      .then((res) => {
        setPoliticians(res.data.politicians);
      })
      .catch((err) => console.log(err));
  }, [election]);

  // detail fields of the election
  const fields = makeFields(election, [
    { name: "Locale", key: "state", link: false },
    { name: "District", key: "district", link: false },
    { name: "Type of Election", key: "branch", link: false },
    { name: "Date of Election", key: "date", link: false },
    { name: "Incumbent", key: "incumbent", link: false },
  ]);

  // display election details, the politicians running, 
  // poll data, and a Google Map
  return (
    <Container>
      {/* election instance attributes */}
      <Row>
        <DetailView header={header} fields={fields} />
      </Row>

      {/* politicians running */}
      <Row className="mx-auto">
        {politicians ? (
          <>
            <h2>Politicians</h2>
            <GridView 
              list={politicians}
              Card={PoliticianDetailCard}
            ></GridView>
          </>
        ) : (
          <></>
        )}
      </Row>

      {/* poll pie chart */}
      <Row className="electionPollChartRow">
        {pollData ? (
          <>
            <h2>Poll Percentages</h2>
            <PieChart pollData={pollData} />
          </>
        ) : (
          <></>
        )}
      </Row>
      {/* map of locale */}
      <Row>
        <h2>Location</h2>
        <DistrictMap districtId={election.state} />
      </Row>
    </Container>
  );
}

export default ElectionDetail;
