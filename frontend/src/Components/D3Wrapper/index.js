import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

export default function D3Wrapper(props) {

    const { chart, data, width, height, className } = props;

    const d3Container = useRef(null);

    useEffect(
        () => {
          const svg = d3.select(d3Container.current);
          chart(svg, data, {width, height});
        }
    );

    return (
        <svg
            className={className}
            ref={d3Container} />
    )

}