import React, { useState, useEffect } from "react";
import { DetailView, makeFields } from "../Detail";
import LinkCard from "../LinkCard";
import TableView from "../TableView";
import { Container, Row, Col } from "react-bootstrap";
import malePlaceholderImg from "../../Images/politician.png";
import femalePlaceholderImg from "../../Images/fem_politician.png";
import {
  policyFieldsLists, socialFieldsLists, basicFieldsList,
} from "./constant.js";
import { getElectionById, getPolicies } from "../../Library/Api";
import "./PoliticianDetail.css";
import { TwitterFeed } from "../TwitterFeed";

// page to display all the info about a politician instance
function PoliticianDetail(props) {
  const { politician } = props;
  const [election, setElection] = useState(undefined);
  const [policies, setPolicies] = useState(undefined);

  // get the election tied to the politician
  useEffect(() => {
    politician.incumbent = politician.incumbent === false ? "False" : "True";
    getElectionById(politician["election id"])
      .then((res) => setElection(res.data))
      .catch((err) => console.log(err));
  }, [politician]);

  // get the policies tied to the politician
  useEffect(() => {
    getPolicies({ politician_id: politician.id })
      .then((res) => setPolicies(res.data.policies))
      .catch((err) => console.log(err));
  }, [politician]);

  // set a default image if no image
  var photo = politician.photo;
  if (photo === "") {
    photo =
      politician.gender === "F" ? femalePlaceholderImg : malePlaceholderImg;
  }

  // header for the deatil page with party and image
  const header = (
    <Container className="politician-header">
      <h2 className="politician-name">{politician.name}</h2>
      <h3>
        {politician.party === "DEM" ? "Democrat" : politician.party === "REP"
          ? "Republican" : politician.party === "LIB" ? "Libertarian"
          : politician.party === "IND"
          ? "Independent" : "Other"}
      </h3>
      <img
        className="politician-portrait"
        alt="Politician Portrait"
        src={photo}
        onError={(e) => {
          e.target.onerror = null;
          e.target.src = politician.gender === "F" ? femalePlaceholderImg
            : malePlaceholderImg;
        }}
        width="400rem"
      ></img>
      <h3>{politician.position}</h3>
    </Container>
  );

  // make the fields of the attributes
  const basicFields = makeFields(politician, basicFieldsList);
  const socialMediaFields = makeFields(politician, socialFieldsLists);
  const policyFields = policyFieldsLists;

  // display deatils about the politician, the politician's policies, the
  // election the politician is running in, and the politician's twitter feed
  return (
    <Row>
      {/* politician instance attributes */}
      <Col sm={6}>
        <DetailView
          header={header}
          fields={[...basicFields, ...socialMediaFields]}
        />
      </Col>
      {/* politiciain's policies */}
      <Col sm={6}>
        {policies ? (
          <>
            <h2>Policies</h2>
            <TableView
              list={policies}
              fields={policyFields}
              path="/policy"
            ></TableView>
          </>
        ) : (
          <></>
        )}
        {/* election politician is running in */}
        {election ? (
          <div className="electionCard">
            <LinkCard
              link={"/election/" + politician["election id"]}
              name={"Running in the " + election.branch + " election."}
              description={"Click here to learn more!"}
            ></LinkCard>
          </div>
        ) : (
          <></>
        )}
        {/* Twitter feed of the politician */}
        {politician.twitter ? (
          <>
            <TwitterFeed twitterHandle={politician.twitter} />
          </>
        ) : (
          <></>
        )}
      </Col>
    </Row>
  );
}

export { PoliticianDetail };
