

const policyFieldsLists = [
  { name: "Name", key: "name" },
  { name: "Level", key: "level" },
];

const socialFieldsLists = [
  { name: "Twitter", key: "twitter", link: false },
  { name: "Facebook", key: "facebook", link: true },
];

const basicFieldsList = [
  { name: "Name", key: "name", link: false },
  { name: "Party", key: "party", link: false },
  { name: "Gender", key: "gender", link: false },
  { name: "Finances", key: "contributions", link: false, currency: true },
  { name: "Website", key: "website", link: true },
  { name: "Incumbent", key: "incumbent", link: false },
  { name: "Address", key: "address", link: false },
];

export { policyFieldsLists, socialFieldsLists, basicFieldsList };