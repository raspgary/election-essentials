import React, { useEffect, useState } from "react";
import WordCloud from "react-wordcloud";
import { getPolicyWordFrequency } from "../../Library/Api";
import Loading from "../Loading";

function PolicyWordCloud() {
  const [wordFrequency, setWordFrequency] = useState(undefined);

  useEffect(() => {
    getPolicyWordFrequency().then((res) => {
      setWordFrequency(res.data);
    });
  }, []);

  const options = {
    rotations: 2,
    rotationAngles: [-90, 0],
    fontSizes: [5, 60],
    fontFamily: "helvetica",
  };

  const size = [965, 600];

  const wordCloud = wordFrequency ? (
    <WordCloud options={options} words={wordFrequency} size={size} />
  ) : (
    <Loading />
  );

  return <> {wordCloud} </>;
}

export default PolicyWordCloud;
