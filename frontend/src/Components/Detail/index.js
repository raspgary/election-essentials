import React from "react";

import { Container, Row, ListGroup } from "react-bootstrap";
import Field from "../Field";

import "./Detail.css";


// gives the fields some props to take from a json object
function makeFields(politician, fields) {
  return fields.map((field, id) => (
    <Field
      key={field.name}
      name={field.name}
      link={field.link}
      value={politician[field.key]}
      currency={field.currency}
    />
  ));
}

// a component that shows details of model instances
function DetailView(props) {
  let { header, fields } = props;

  // has a header and a table info
  return (
    <Container className="detail-view">
      <Row>{header}</Row>
      <Row>
        <Container>
          <ListGroup>{fields}</ListGroup>
        </Container>
      </Row>
    </Container>
  );
}

export { Field, DetailView, makeFields };
