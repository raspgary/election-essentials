import React, { useState } from "react";
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link, useHistory, withRouter } from "react-router-dom";
import malePlaceholderImg from "../../Images/politician.png";
import "./Navbar.css";

import {navlinks,} from "./constant.js";

// navigation bar on the top of web app that goes to different pages
// links turn in to an hamburger when the screen is collapsed
function NavigationBar() {
  const [search, setSearch] = useState();
  const history = useHistory();

  // search form
  const searchAll = (e) => {
    e.preventDefault();
    let params = new URLSearchParams({ query: search });
    // navigate to the search page
    history.push("/search?" + params);
  };

  return (
    <Navbar expand="sm" >
      <Navbar.Brand href="/">
      <img
        alt=""
        src={malePlaceholderImg}
        width="50"
        height="50"
        className="d-inline-block align-top"
      />{' '}
        The Politician
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse variant="dark">
        <Nav className="mr-auto">
          {navlinks.map((ele, i) => (
            <Link to={ele.link} id={ele.id} component={Nav.Link}>{ele.value}</Link>
          ))}
        </Nav>
        <Form onSubmit={searchAll} inline>
          <FormControl
            type="text"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            placeholder="Search"
            className="mr-sm-2"
          />
          <Button type="submit" variant="outline-primary">
            Search
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default withRouter(NavigationBar);
