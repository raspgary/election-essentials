const navlinks = [
  { id: "homeNav", value: "Home", link: "/" },
  { id: "politicianNav", value: "Politicians", link: "/politician" },
  { id: "electionNav", value: "Elections", link: "/election" },
  { id: "policyNav", value: "Policies", link: "/policy" },
  { id: "visualNav", value: "Visualizations", link: "/visualizations" },
  { id: "providNav", value: "Provider Visualizations", link: "/providers" },
  { id: "aboutNav", value: "About", link: "/about" },
];

export { navlinks };
