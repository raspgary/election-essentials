import React from "react";
import { Button, Table } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";
import { Link } from "react-router-dom";

// table row to display info
function TableRow(props) {
  const { fields, data, path, id, highlight } = props;

  // displays the field names and info of the instance
  return (
    <tr>
      {fields.map((f) => (
        <td key={f.key}>
          {highlight ? (
            <Highlight attribute={f.key} hit={data} />
          ) : (
            data[f.key]
          )}
        </td>
      ))}
      <td>
        <Link component={Button} variant="primary" to={path + "/" + id}>
          More info
        </Link>
      </td>
    </tr>
  );
}

// table used to display elections and policies on model pages
function TableView(props) {
  const { fields, list, path, highlight } = props;

  // make a table out of a list of rows
  return (
    <Table>
      <thead>
        <tr>
          {fields.map((f) => (
            <th key={f.key}> {f.name} </th>
          ))}
          <th></th>
        </tr>
      </thead>
      <tbody>
        {list.map((ele, id) => (
          <TableRow
            highlight={highlight}
            key={id}
            fields={fields}
            data={ele}
            id={ele.id}
            path={path}
          />
        ))}
      </tbody>
    </Table>
  );
}

export default TableView;
