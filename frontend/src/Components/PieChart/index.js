import React from "react";
import { Doughnut } from "react-chartjs-2";

// a pie chart to display poll data of elections
function PieChart(props) {
  const { pollData } = props;

  // get the poll values for the election
  function getValues(jsonObj) {
    var values = [];
    for (var key in jsonObj) {
      values.push(jsonObj[key]);
    }
    return values;
  }
  const values = getValues(pollData);

  // format the data for the pie chart API with the labels of the polition
  // names, the poll results, and some colors
  const data = {
    labels: Object.keys(pollData),
    datasets: [
      {
        label: "# of Votes",
        data: values,
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };
  return <Doughnut data={data} />;
}

export default PieChart;
