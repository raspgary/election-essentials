import sanjay from "../../Images/sanjay.jpg";
import aniket from "../../Images/aniket.jpg";
import deepthi from "../../Images/deepthi.jpg";
import gary from "../../Images/gary.jpg";
import michael from "../../Images/michael.jpg";
import postman from "../../Images/postman.png";
import gitlab from "../../Images/gitlab.png";

import react from "../../Images/react.png";
import bootstrap from "../../Images/bootstrap.png";
import flask from "../../Images/flask.png";
import docker from "../../Images/docker.png";
import aws from "../../Images/aws.png";
import algolia from "../../Images/algolia.png";
import d3 from "../../Images/d3.png";

import google from "../../Images/google.png";
import propublica from "../../Images/propublica.png";
import fivethirtyeight from "../../Images/fivethirtyeight.png";

// fields for contributors
const getContributors = (commits, issues, unitTests) => [
  {
    image: sanjay,
    name: "Sanjay Yepuri",
    role: "Frontend Developer",
    bio: "I am a senior studying computer science and I enjoy watching anime.",
    commits: commits["sanjay.yepuri@gmail.com"],
    issues: issues["sanjayyepuri"],
    unittests: unitTests["sanjayyepuri"],
    linkedin: "https://www.linkedin.com/in/sanjay-yepuri/",
  },
  {
    image: aniket,
    name: "Aniket Joshi",
    role: "Techlead",
    bio: "I am a senior studying computer science and I enjoy watching anime.",
    commits: commits["aniket.joshi@utexas.edu"],
    issues: issues["aniket.joshi217"],
    unittests: unitTests["aniket.joshi217"],
    linkedin: "https://www.linkedin.com/in/anktjsh/",
  },
  {
    image: deepthi,
    name: "Deepthi Pittala",
    role: "Backend Developer",
    bio: "I am a senior studying computer science and I enjoy watching anime.",
    commits: commits["pittala.deepthi@utexas.edu"],
    issues: issues["deepthi54"],
    unittests: unitTests["deepthi54"],
    linkedin: "https://www.linkedin.com/in/deepthipittala/",
  },
  {
    image: gary,
    name: "Gary Wang",
    role: "Frontend Developer",
    bio: "I am a junior studying computer science and I enjoy watching anime.",
    commits: commits["garywqc@utexas.edu"],
    issues: issues["raspgary"],
    unittests: unitTests["raspgary"],
    linkedin: "https://www.linkedin.com/in/gary-wang-utcs/",
  },
  {
    image: michael,
    name: "Michael Liu",
    role: "Backend Developer",
    bio: "I am a senior studying computer science and I enjoy watching anime.",
    commits: commits["mlglal1@gmail.com"],
    issues: issues["Ha3vn"],
    unittests: unitTests["Ha3vn"],
    linkedin: "https://www.linkedin.com/in/michaelmliu/",
  },
];

// fields for Postaman API and Gitlab URL
const links = [
  {
    image: postman,
    link: "https://documenter.getpostman.com/view/12889746/TVRd8B7M",
    name: "Postman REST API",
    description: "",
  },
  {
    image: gitlab,
    link: "https://gitlab.com/raspgary/election-essentials",
    name: "Gitlab Repo",
    description: "",
  },
];

// fields for the tools used
const tools = [
  {
    image: react,
    link: "https://reactjs.org/docs/getting-started.html",
    name: "React",
    description: "Built the frontend of the web app",
  },
  {
    image: bootstrap,
    link: "https://react-bootstrap.github.io/",
    name: "React Bootstrap",
    description: "Styled the frontend of the web app",
  },
  {
    image: flask,
    link: "https://flask.palletsprojects.com/en/1.1.x/",
    name: "Flask",
    description: "Built the server side REST API",
  },
  {
    image: aws,
    link: "https://docs.aws.amazon.com/",
    name: "AWS",
    description: "Hosts the web app and database",
  },
  {
    image: docker,
    link: "https://docs.docker.com/",
    name: "Docker",
    description: "Cotainerized the frontend and backend",
  },
  {
    image: postman,
    link: "https://learning.postman.com/docs/getting-started/introduction/",
    name: "Postman",
    description: "Document the REST API endpoints",
  },
  {
    image: gitlab,
    link: "https://docs.gitlab.com/",
    name: "Gitlab",
    description: "Source control and issue tracking",
  },
  {
    image: algolia,
    link: "https://www.algolia.com/doc/",
    name: "Algolia",
    description: "Search, Sort, and Filtering Items",
  },
  {
    image: d3,
    link: "https://github.com/d3/d3/wiki",
    name: "D3",
    description: "Visualizations",
  },
];

// fields for the data sources used
const sources = [
  {
    image: google,
    link: "https://developers.google.com/civic-information",
    name: "Google Civic Information API",
    description: "Scraped Politician and Election Info by Voter Location",
  },
  {
    image: propublica,
    link: "https://www.propublica.org/datastore/api/campaign-finance-api",
    name: "ProPublica Campaign Finance API",
    description: "Scraped Politician Campaign Finance and Personal Info",
  },
  {
    image: fivethirtyeight,
    link: "https://data.fivethirtyeight.com/",
    name: "FiveThirtyEight Federal Elections API",
    description: "Scraped Info for the Senate and House Federal Elections",
  },
];

export { getContributors, links, tools, sources };
