import React, { useState, useEffect } from "react";
import "./AboutProfiles.css";
import { getContributors, links, tools, sources } from "./constants";
import LinkCard from "../LinkCard";
import GridView from "../GridView";
import AboutDetailCard from "../AboutDetailCard";
import { Container, Row, Col, Card } from "react-bootstrap";
import { getUnitTests, getIssues, getUsers } from "../../Library/Gitlab.js";

function AboutProfiles() {
  // state variables for Gitlab info
  const [commitsCount, setCommitsCount] = useState(0);
  const [issuesCount, setIssuesCount] = useState(0);
  const [unitTestsCount, setUnitTestsCount] = useState(0);
  const [commits, setCommits] = useState({
    "sanjay.yepuri@gmail.com": 0,
    "aniket.joshi@utexas.edu": 0,
    "pittala.deepthi@utexas.edu": 0,
    "garywqc@utexas.edu": 0,
    "mlglal1@gmail.com": 0,
  });
  const [issues, setIssues] = useState({
    sanjayyepuri: 0,
    "aniket.joshi217": 0,
    deepthi54: 0,
    raspgary: 0,
    Ha3vn: 0,
  });

  const [unitTests] = useState({
    sanjayyepuri: 0,
    "aniket.joshi217": 26,
    deepthi54: 25,
    raspgary: 33,
    Ha3vn: 0,
  });

  // get all the Gitlab info for commits, issues, and unit tests
  useEffect(() => {
    getUnitTests(unitTests, setUnitTestsCount);
    getUsers(commits, setCommits, setCommitsCount);
    getIssues(issues, setIssues, setIssuesCount);
  }, [unitTests, commits, issues]);

  // get porfile info from constant.js
  const contributors = getContributors(commits, issues, unitTests);

  // display the mission, team profiles, and links to Gitlab repo, Postman API,
  // tools used, and data sources
  return (
    <Container fluid id="aboutprofiles">
      {/* Mission */}
      <Row>
        <Col id="missionHeader" className="aboutHeader">
          Our Mission
        </Col>
      </Row>
      <Row>
        <Col>
          With The Politician, our goal is to build a website that aggregates
          information about politicians in the upcoming federal elections and
          their policies. Users will come to learn about what the politicians in
          their area are saying and the platforms that they are running on. This
          way our users can be better informed and feel comfortable casting
          votes for the people they trust.
        </Col>
      </Row>

      {/* Team Profiles */}
      <Row>
        <Col className="aboutHeader">The Team</Col>
      </Row>
      <GridView list={contributors} Card={AboutDetailCard} />

      {/* Total Gitlab Metrics */}
      <Row>
        <Col md="auto">
          <Card style={{ width: "30vh" }} id="commitBox">
            <Card.Body>
              <Card.Title>Total Gitlab Info:</Card.Title>
              <Card.Text>Commits: {commitsCount}</Card.Text>
              <Card.Text>Issues: {issuesCount}</Card.Text>
              <Card.Text>Unit Tests: {unitTestsCount}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      {/* Gitlab repo and Postman API links */}
      <Row>
        <Col className="aboutHeader">Project Info</Col>
      </Row>
      <GridView list={links} Card={LinkCard} />

      {/* Tools Links */}
      <Row>
        <Col className="aboutHeader">Tools</Col>
      </Row>
      <GridView list={tools} Card={LinkCard} />

      {/* Data Source Links */}
      <Row>
        <Col className="aboutHeader">Data Sources</Col>
      </Row>
      <GridView list={sources} Card={LinkCard} />
    </Container>
  );
}
export default AboutProfiles;
