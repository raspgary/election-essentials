import React from "react";
import { Container } from "react-bootstrap";
import "./GridView.css";

// a grid component to display politicians on the model page
function GridView(props) {
  const { Card, list, highlight } = props;

  return (
    <Container>
      <div className="grid">
        {list.map((ele, i) => (
          <div key={i} className="item">
            <Card highlight={highlight} data={ele} id={i} />
          </div>
        ))}
      </div>
    </Container>
  );
}

export default GridView;
