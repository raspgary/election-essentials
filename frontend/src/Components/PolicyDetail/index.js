import React, { useState, useEffect } from "react";
import { DetailView, makeFields } from "../Detail";
import { Col, Container, Row } from "react-bootstrap";
import { PoliticianDetailCard } from "../PoliticianDetailCard";
import { getPoliticianById } from "../../Library/Api";
import placeholderImg from "../../Images/scale.png";
import "./PolicyDetail.css";

// page to display all the info about a policy instance
function PolicyDetail(props) {
  const { policy } = props;
  const [politician, setPolitician] = useState(undefined);

  // header for the policy instance page
  const header = (
    <Container>
      <h2> {policy.name} </h2>
      <Row>
        <Col>
          <img
            id="policyImage"
            className="politician-portrait"
            alt="Politician Portrait"
            src={policy.image || placeholderImg}
            onError={(e) => {
              e.target.onerror = null;
              e.target.src = placeholderImg;
            }}
            width="400rem"
          ></img>
        </Col>
        <Col style={{ "margin-left": "8vw", "margin-bottom": "4vh" }}>
          {/* politician involved */}
          {politician ? (
            <>
              <PoliticianDetailCard data={politician}></PoliticianDetailCard>
            </>
          ) : (
            <></>
          )}
        </Col>
      </Row>
    </Container>
  );

  // get the politician for the policy
  useEffect(() => {
    getPoliticianById(policy["politician id"])
      .then((res) => {
        setPolitician(res.data);
      })
      .catch((err) => console.log(err));
  }, [policy]);

  // get the fields for the attributes of the policy
  const fields = makeFields(policy, [
    { name: "Politician", key: "politician", link: false },
    { name: "Level", key: "level", link: false },
    { name: "Stance", key: "stance", link: true },
    { name: "News", key: "news", link: true },
    { name: "Party", key: "party", link: false },
    { name: "Slogan", key: "subheader", link: false },
    { name: "Description", key: "description", link: false },
  ]);

  // display details about the policy and the politician it belongs to
  return (
    <Container fluid className="policyContainer">
      {/* policy instance attributes */}
      <Row>
        <DetailView header={header} fields={fields} />
      </Row>
    </Container>
  );
}

export default PolicyDetail;
