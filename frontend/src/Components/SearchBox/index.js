import React from "react";
import { Form, InputGroup } from "react-bootstrap";
import { connectSearchBox, PoweredBy } from "react-instantsearch-dom";

const SearchBox = connectSearchBox(({ currentRefinement, refine }) => (
  <Form.Group>
    <InputGroup>
      <Form.Control
        size="lg"
        type="text"
        placeholder="Search"
        value={currentRefinement}
        onChange={(e) => refine(e.currentTarget.value)}
      />
      <InputGroup.Append>
        <InputGroup.Text>
          <PoweredBy />
        </InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>
  </Form.Group>
));

export { SearchBox };
