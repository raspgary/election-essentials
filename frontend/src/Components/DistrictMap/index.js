import React from "react";

import D3Wrapper from "../D3Wrapper";

import * as topojson from "topojson-client";
import * as d3 from "d3";
import * as d3Geo from "d3-geo";
import us from "./us.json";
import district_top from "./us-congress-116-simple.json";
import state_ids from "./state_names.json";


import "./DistrictMap.css";

// display a district map with Google Map API
function DistrictMap(props) {
  const { districtId, data } = props;

  const chart = async (svg) => {
    svg.attr("width", 965).attr("height", 600);
    svg.attr("viewBox", [0, 0, 965, 600]);

    const states = topojson.feature(us, us.objects.states);
    const districts = topojson.feature(
      district_top,
      district_top.objects.districts
    );

    const path = d3.geoPath(d3Geo.geoAlbersUsa());

    const getStateCode = (did) => {
      const sid = parseInt(did.properties.STATEFP);
      let id = parseInt(did.properties.CD116FP);

      if (id === 0) {
        id = id + 1;
      }

      return `${state_ids[sid].state_id}-${id}`;
    };

    const select_dist = (did, i) => {
      const state_code = getStateCode(did);

      return state_code === districtId || districtId === "US"
        ? "red"
        : "transparent";
    };
    svg
      .append("g")
      .selectAll("path")
      .data(states.features)
      .join("path")
      .attr("fill", "steelblue")
      .attr("stroke", "white")
      .attr("d", path);

    const districtPath = svg
      .append("g")
      .selectAll("path")
      .data(districts.features)
      .join("path")
      .attr("fill", select_dist)
      .attr("stroke", "white")
      .attr("d", path);

    if (!districtId) {
      const tooltip = svg.append("g");

      const callout = (g, value) => {
        if (!value) return g.style("display", "none");

        g.style("display", null)
          .style("pointer-events", "none")
          .style("font", "10px sans-serif");

        const path = g
          .selectAll("path")
          .data([null])
          .join("path")
          .attr("fill", "white")
          .attr("stroke", "black");

        const text = g
          .selectAll("text")
          .data([null])
          .join("text")
          .call((text) =>
            text
              .selectAll("tspan")
              .data((value + "").split(/\n/))
              .join("tspan")
              .attr("x", 0)
              .attr("y", (d, i) => `${i * 1.1}em`)
              .style("font-weight", (_, i) => (i ? null : "bold"))
              .text((d) => d)
          );

        const { x, y, width: w, height: h } = text.node().getBBox();
        text.attr("transform", `translate(${-w / 2},${15 - y})`);
        path.attr(
          "d",
          `M${-w / 2 - 10 + x - x},5H-5l5,-5l5,5H${w / 2 + 10}v${h + 20}h-${
            w + 20
          }z`
        );
      };

      var mouseover = function (d) {
        d3.select(this).attr("stroke", "#000").attr("stroke-width", 3).raise();
      };
      var mousemove = function (e) {
        const [x, y] = d3.pointer(e, this);

        let text;
        const state_code = getStateCode(e.srcElement.__data__);
        if (data[state_code]) {
          const politicians = data[state_code].join("\n");
          text = state_code + '\n' + politicians;
        } else {
          text = state_code;
        }

        tooltip
          .attr("transform", `translate(${x},${y})`)
          .call(callout, text);
      };
      var mouseleave = function (d) {
        tooltip.call(callout, null);
        d3.select(this).attr("stroke", "white").attr("stroke-width", 1).lower();
      };

      districtPath
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave);
    }
  };

  return <D3Wrapper chart={chart} height={600} width={965} />;
}

export default DistrictMap;
