import React from "react";
import AboutProfiles from "../../Components/AboutProfiles";
import { Container } from "react-bootstrap";

// page to render the about page
function AboutPage() {
  return (
    <Container id="aboutPage">
      <AboutProfiles />
    </Container>
  );
}

export default AboutPage;
