import React from "react";
import scale from "../../Images/scale.png";
import politician from "../../Images/politician.png";
import ballot from "../../Images/ballot.png";
import "./LandingPage.css";
import { Container, Row, Col } from "react-bootstrap";

import LinkCard from "../../Components/LinkCard";
import GridView from "../../Components/GridView";

// page to render landing page with useful info and link cards
function LandingPage() {
  // json for rendering link cards to model pages
  const cards = [
    {
      image: politician,
      link: "/politician",
      name: "Politicians",
      description: "",
    },
    {
      image: ballot,
      link: "/election",
      name: "Elections",
      description: "",
    },
    {
      image: scale,
      link: "/policy",
      name: "Policies",
      description: "",
    },
  ];

  // display a slogan, some about info, and link cards to model pages
  return (
    <Container fluid>
      {/* slogan */}
      <Row id="landingRow1" className="landingRow">
        <Col sm={12} id="landingCol1" className="landingCol">
          <p id="slogan1">The Politician</p>
          <p id="slogan2">Your One Stop Shop to Politics</p>
        </Col>
      </Row>
      {/* about info */}
      <Row id="landingRow2">
        <div id="slogan3" style={{ width: "100vw" }}>
          <p>About Us</p>
        </div>
        <div id="slogan4" style={{ width: "100vw" }}>
          <p>
            With The Politician, our goal is to build a website that aggregates
            information about politicians in the upcoming federal elections and
            their policies. Users will come to learn about what the politicians
            in their area are saying and the platforms that they are running on.
            This way our users can be better informed and feel comfortable
            casting votes for the people they trust.
          </p>
        </div>
      </Row>
      {/* model page link cards */}
      <Row id="landingRow3" className="landingRow">
        <div id="slogan3" style={{ width: "100vw" }}>
          <h1 id="clickBelowLabel">
            Click below to learn about elections near you!
          </h1>
        </div>
        <Container>
          <GridView list={cards} Card={LinkCard} />
        </Container>
      </Row>
    </Container>
  );
}

export default LandingPage;
