import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import PolicyWordCloud from "../../Components/PolicyWordCloud";
import ScrollableBarChart from "../../Components/ScrollableBarChart";
import DistrictMap from "../../Components/DistrictMap";

import { getPoliticianByDistrict } from "../../Library/Api";
import { getPoliticianFinances } from "../../Library/Api";

import "./visualizations.css";
import Loading from "../../Components/Loading";

// page to render the visualizations page
function VisualizationsPage() {
  const [districtMap, setDistrictMap] = useState(null);
  const [financeData, setFinanceData] = useState(null);

  useEffect(() => {
    getPoliticianByDistrict()
      .then((res) => {
        setDistrictMap(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    getPoliticianFinances().then((res) => {
      setFinanceData(res.data);
    });
  }, []);

  return (
    <Container className="visualizations">
      <>
        <h1 className="page-title">Visualizations</h1>
        <h3 className="page-title"> Common Policy Words </h3>
        <p>
          Our team scraped each of the candidate's websites for policies and
          created a word cloud based on the frequency words used in the
          descriptions and titles.
        </p>
        <PolicyWordCloud />
        <h3 className="page-title"> Campaign Finances </h3>
        <p>
          This barchart ranks each of our candidates based on the amount of
          funding they had available for their campaigns in dollars. Here we are
          showing the top 50.
        </p>
        <ScrollableBarChart width={140} data={financeData} />
        <h3 className="page-title"> Map of 116th Congressional District </h3>
        <p>
          Here we have a map of the 116th Congressional District. Each of the
          districts elects a member to house. The boundary data was sourced from
          the U.S. Census Bureau. Hover to see the House candidates running in
          each district.{" "}
        </p>
        {districtMap ? <DistrictMap data={districtMap} /> : <Loading />}
      </>
    </Container>
  );
}

export default VisualizationsPage;
