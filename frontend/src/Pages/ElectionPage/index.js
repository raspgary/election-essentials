import React from "react";
import { searchClient, TableHits } from "../../Library/Algolia";
import { SearchBox } from "../../Components/SearchBox";
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
} from "react-instantsearch-dom";
import { Col, Container, Row } from "react-bootstrap";
import LoadingPage from "../LoadingPage";

// fields on the table rows
const fields = [
  { name: "Branch", key: "branch" },
  { name: "Date", key: "date" },
  { name: "Locale", key: "state" },
];

const electionPage = (
  <Container>
    <h1 className="page-title"> Elections </h1>
    <InstantSearch searchClient={searchClient} indexName="dev_elections">
      <Container className="search-container">
        {/* serach box */}
        <SearchBox />
      </Container>
      <Container>
        <Row>
          <Col className="filter" sm={3}>
            <h5>Branch</h5>
            {/* filter boxes */}
            <RefinementList attribute="branch" />
            {/* sort dropdown */}
            <SortBy
              items={[
                { value: "dev_elections", label: "None" },
                { value: "dev_elections_state_asc", label: "State (asc.)" },
                {
                  value: "dev_elections_state_desc",
                  label: "State (desc.)",
                },
              ]}
              defaultRefinement={"dev_elections"}
            />
          </Col>
          <Col lg={9}>
            {/* content table and pagination */}
            <TableHits fields={fields} path="/election" />
            <Pagination />
          </Col>
        </Row>
      </Container>
    </InstantSearch>
  </Container>
);

// page to render the election model page with search, sort,
// filter, and pagination capabilities
function ElectionPage() {
  return (
    <Container id="electionsPage">
      <LoadingPage page={electionPage} />
    </Container>
  );
}

export default ElectionPage;
