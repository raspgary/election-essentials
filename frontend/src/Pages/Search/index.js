import React, { useState } from "react";
import { searchClient, TableHits, GridHits } from "../../Library/Algolia";
import { SearchBox } from "../../Components/SearchBox";
import {
  InstantSearch,
  Pagination,
  Index,
  HitsPerPage,
} from "react-instantsearch-dom";
import { PoliticianDetailCard } from "../../Components/PoliticianDetailCard";
import { Container, Row, Col } from "react-bootstrap";
import { useLocation, useHistory } from "react-router-dom";
import qs from "qs";

import "./search.css";

const ModelResults = ({ indexName, children }) => {
  return (
    <Index indexName={indexName}>
      <Container>
        <Row className="result-container">{children}</Row>
        <Row className="left-align">
          <Col sm={2}>
            <HitsPerPage
              defaultRefinement={3}
              items={[{ value: 3, label: "Show 3 Results" }]}
            />
          </Col>
          <Col sm={10}>
            <Pagination />
          </Col>
        </Row>
      </Container>
    </Index>
  );
};
const urlToSearchState = ({ search }) => qs.parse(search.slice(1));

function SearchPage() {
  // fields that are displayed for the models
  const policyFields = [
    { name: "Name", key: "name" },
    { name: "Level", key: "level" },
  ];

  const electionFields = [
    { name: "Branch", key: "branch" },
    { name: "Date", key: "date" },
    { name: "Locale", key: "state" },
  ];

  // parse the query search parameters from the url

  const location = useLocation();
  const history = useHistory();
  const [searchState, setSearchState] = useState(urlToSearchState(location));

  // update the url to reflect the current search state
  const onSearchStateChanged = (newSearchState) => {
    history.push("/search?" + qs.stringify(newSearchState));
    setSearchState(newSearchState);
  };

  return (
    <Container  className="search">
      <h1 className="page-title"> Search </h1>
      <InstantSearch
        searchClient={searchClient}
        onSearchStateChange={onSearchStateChanged}
        searchState={searchState}
        indexName="dev_politicians"


      >
        <SearchBox />
        <hr class="solid"></hr>
        <h2>Politicians</h2>
        <ModelResults indexName="dev_politicians">
          <GridHits Card={PoliticianDetailCard} />
        </ModelResults>
        <hr class="solid"></hr>
        <h2>Election</h2>
        <ModelResults indexName="dev_elections">
          <TableHits fields={electionFields} path="election" />
        </ModelResults>
        <hr class="solid"></hr>
        <h2>Policies</h2>
        <ModelResults indexName="dev_policies">
          <TableHits fields={policyFields} path="policy" />
        </ModelResults>
      </InstantSearch>
    </Container>
  );
}

export default SearchPage;
