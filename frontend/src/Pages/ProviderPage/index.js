import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";

import WordCloud from "react-wordcloud";
import PieChart from "../../Components/PieChart";
import Loading from "../../Components/Loading";
import { getCountiesText, getDisasters, getPrograms } from "../../Library/Api";
import ScrollableBarChart from "../../Components/ScrollableBarChart";

// page to render the visualizations page
function ProviderPage() {
  const [county, setCounty] = useState(null);
  const [disaster, setDisaster] = useState(null);
  const [programs, setPrograms] = useState(null);

  useEffect(() => {
    const getProviderData = [getCountiesText(), getDisasters(), getPrograms()];

    Promise.all(getProviderData)
      .then(([countyRes, disasterRes, programRes]) => {
        setCounty(countyRes.data);
        var disasterPoll = {};
        for (var i in disasterRes.data) {
          var disasterEle = disasterRes.data[i];
          if (disasterEle["x"] !== "Other") {
            disasterPoll[disasterEle["x"]] = Math.trunc(disasterEle["y"] * 100);
          }
        }
        setDisaster(disasterPoll);
        setPrograms(programRes.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const options = {
    rotations: 2,
    rotationAngles: [-90, 0],
    fontSizes: [5, 45],
    fontFamily: "helvetica",
  };
  const size = [965, 600];

  return (
    <Container className="visualizations">
      <h1 className="page-title">Provider Visualizations</h1>
      <h3 className="page-title"> Counties by Frequency of Disasters </h3>
      <p>
        This visualization ranks the top 50 counties by the number of disasters
        that hd occured. The scale of the words represent the relative number of
        disasters that occur.
      </p>
      {county ? (
        <WordCloud size={size} options={options} words={county} />
      ) : (
        <Loading />
      )}

      <h3 className="page-title"> Most Common Disasters in the US </h3>
      <p>
        These are the most common disasters that occur in the US by frequency.
      </p>
      {disaster ? <PieChart pollData={disaster} /> : <Loading />}
      <h3 className="page-title">
        {" "}
        Top 20 Most Funded Disaster Relief Progams{" "}
      </h3>
      <p>
        These Disaster Relief Programs are the most funded. The graph shows the
        funding in dollars.
      </p>
      <ScrollableBarChart width={450} data={programs} />
    </Container>
  );
}

export default ProviderPage;
