import React, { useEffect, useState } from "react";

import { Loading } from "../../Components/Loading";
import { Container } from "react-bootstrap";

// display loading spinner before the page
function LoadingPage(props) {
  const { page } = props;
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    // show loading spinner for 1 second
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 1000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Container>
      {isLoading ? <Loading /> : <></>}
      {!isLoading ? <>{page}</> : <></>}
    </Container>
  );
}

export default LoadingPage;
