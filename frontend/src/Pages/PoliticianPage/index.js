import React from "react";
import { searchClient, GridHits } from "../../Library/Algolia";
import { SearchBox } from "../../Components/SearchBox";
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
} from "react-instantsearch-dom";
import { PoliticianDetailCard } from "../../Components/PoliticianDetailCard";
import LoadingPage from "../LoadingPage";
import { Col, Container, Row } from "react-bootstrap";

// display the politician model page with a grid, serach, sort,
// filter, and pagination
function PoliticianPage() {
  const politicianPage = (
    <Container>
      <h1 className="page-title"> Politicians </h1>
      <InstantSearch
        id="instant-search"
        searchClient={searchClient}
        indexName="dev_politicians"
      >
        <Container className="search-container">
          <SearchBox />
        </Container>
        <Container>
          <Row>
            <Col sm={3} className="filter">
              <h5>District</h5>
              {/* filter boxes */}
              <RefinementList attribute="district" searchable />
              <h5>Election Type</h5>
              <RefinementList attribute="election.branch" />
              {/* sort dropdown */}
              <h5>Sort By</h5>
              <SortBy
                items={[
                  { value: "dev_politicians", label: "None" },
                  {
                    value: "dev_politicians_state_asc",
                    label: "State (asc.)",
                  },
                  {
                    value: "dev_politicians_state_desc",
                    label: "State (desc.)",
                  },
                  {
                    value: "dev_politicians_name_asc",
                    label: "Name (asc.)",
                  },
                  {
                    value: "dev_politicians_name_desc",
                    label: "Name (desc.)",
                  },
                ]}
                defaultRefinement={"dev_politicians"}
              />
            </Col>
            <Col lg={9}>
              {/* content table and pagination */}
              <GridHits Card={PoliticianDetailCard} />
              <Pagination />
            </Col>
          </Row>
        </Container>
      </InstantSearch>
    </Container>
  );

  return (
    <Container id="politicianPage">
      <LoadingPage page={politicianPage} />
    </Container>
  );
}

export default PoliticianPage;
