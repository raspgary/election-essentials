import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import ElectionDetail from "../../Components/ElectionDetail";

import { getElectionById } from "../../Library/Api";

// page to render details about an election instance
function ElectionDetailPage() {
  const { id } = useParams();
  const [election, setElection] = useState(undefined);

  // gets the info about an election by ID
  useState(() => {
    getElectionById(id)
      .then((res) => {
        setElection(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  // passes in the elction info to the election detail component
  return (
    <Container style={{ padding: "5rem" }}>
      {election ? <ElectionDetail election={election} /> : <></>}
    </Container>
  );
}

export default ElectionDetailPage;
