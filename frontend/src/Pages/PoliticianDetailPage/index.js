import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { PoliticianDetail } from "../../Components/PoliticianDetail";

import { getPoliticianById } from "../../Library/Api";

// page to render politician instance info
function PoliticianDetailPage() {
  const { id } = useParams();
  const [politician, setPolitician] = useState(undefined);

  // get the info about the politician
  useState(() => {
    getPoliticianById(id)
      .then((pol) => setPolitician(pol.data))
      .catch((err) => console.log(err));
  }, []);

  // send in the politician to the politician detail page
  return (
    <Container style={{ padding: "5rem" }}>
      {politician ? <PoliticianDetail politician={politician} /> : <></>}
    </Container>
  );
}

export default PoliticianDetailPage;
