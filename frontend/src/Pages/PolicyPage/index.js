import React from "react";
import { searchClient, TableHits } from "../../Library/Algolia";
import { SearchBox } from "../../Components/SearchBox";
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
} from "react-instantsearch-dom";
import { Col, Container, Row } from "react-bootstrap";
import LoadingPage from "../LoadingPage";

// fields on the table rows
const fields = [
  { name: "Name", key: "name" },
  { name: "Level", key: "level" },
];

// page to render the policy model page with search,
// filter, sort, and pagination capabilities
function PolicyPage() {
  const policyPage = (
    <Container>
      <h1 className="page-title"> Policies </h1>
      <InstantSearch searchClient={searchClient} indexName="dev_policies">
        <Container className="search-container">
          {/* serach box */}
          <SearchBox />
        </Container>
        <Container>
          <Row>
            <Col className="filter" sm={3}>
              <h5>Level</h5>
              {/* filter boxes */}
              <RefinementList attribute="level" />
              {/* sort dropdown */}
              <SortBy
                items={[
                  { value: "dev_policies", label: "None" },
                  { value: "dev_policies_name_asc", label: "Name (asc.)" },
                  { value: "dev_policies_name_desc", label: "Name (desc.)" },
                ]}
                defaultRefinement={"dev_policies"}
              />
            </Col>
            <Col lg={9}>
              {/* content table and pagination */}
              <TableHits fields={fields} path="/policy" />
              <Pagination />
            </Col>
          </Row>
        </Container>
      </InstantSearch>
    </Container>
  );
  return (
    <Container id="policiesPage">
      <LoadingPage page={policyPage} />
    </Container>
  );
}

export default PolicyPage;
