import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import PolicyDetail from "../../Components/PolicyDetail";

import { getPolicyById, getPoliticianById } from "../../Library/Api";

// page to render policy instance info
function PolicyDetailPage() {
  const { id } = useParams();
  const [policy, setPolicy] = useState(undefined);

  // get the info about the policy
  useState(() => {
    getPolicyById(parseInt(id))
      .then(async (res) => {
        await setPolitician(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  // get the politician for the policy
  async function setPolitician(policy) {
    await getPoliticianById(policy["politician id"])
      .then((res) => {
        policy["politician"] = res.data["name"];
      })
      .catch((err) => {
        console.log(err);
      });

    setPolicy(policy);
  }

  // send in policy info to policy detail component
  return (
    <Container style={{ padding: "5rem" }}>
      {policy ? <PolicyDetail policy={policy} /> : <></>}
    </Container>
  );
}

export default PolicyDetailPage;
