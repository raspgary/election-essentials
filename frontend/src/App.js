import React from "react";
import "./App.css";
import { Route, BrowserRouter } from "react-router-dom";

import AboutPage from "./Pages/AboutPage";
import ElectionPage from "./Pages/ElectionPage";
import ElectionDetailPage from "./Pages/ElectionDetailPage";
import LandingPage from "./Pages/LandingPage";
import PolicyPage from "./Pages/PolicyPage";
import PolicyDetailPage from "./Pages/PolicyDetailPage";
import PoliticianPage from "./Pages/PoliticianPage";
import PoliticianDetailPage from "./Pages/PoliticianDetailPage";
import VisualizationsPage from "./Pages/VisualizationsPage";
import ProviderPage from "./Pages/ProviderPage";

import NavigationBar from "./Components/Navbar";
import SearchPage from "./Pages/Search";

// load everything on the page by url using React router
function App() {
  return (
    <div className="App">
      {/* always have navigation bar at the top */}
      {/* routes to other pages */}
      <BrowserRouter>
        <NavigationBar />
        <div className="spacer"></div>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/about" component={AboutPage} />
        <Route exact path="/election" component={ElectionPage} />
        <Route exact path="/election/:id" component={ElectionDetailPage} />
        <Route exact path="/policy" component={PolicyPage} />
        <Route exact path="/policy/:id" component={PolicyDetailPage} />
        <Route exact path="/politician" component={PoliticianPage} />
        <Route exact path="/search" component={SearchPage} />
        <Route exact path="/visualizations" component={VisualizationsPage} />
        <Route exact path="/providers" component={ProviderPage} />
        <Route
          exact
          path="/politician/:id"
          component={PoliticianDetailPage}
        />
      </BrowserRouter>
    </div>
  );
}

export default App;
