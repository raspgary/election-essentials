import * as api from "./Api";

// unit tests for Election API request
describe("API Election", () => {
  it("all elections should return 471 elections", async () => {
    var val = await api.getElections();
    expect(val.data.elections.length).toEqual(471);
  });
  it("presidential should return 1 election", async () => {
    var val = await api.getElections({ type: "presidential" });
    expect(val.data.elections.length).toEqual(1);
  });
  it("house should return 435 elections", async () => {
    var val = await api.getElections({ type: "house" });
    expect(val.data.elections.length).toEqual(435);
  });
  it("senate should return 35 elections", async () => {
    var val = await api.getElections({ type: "senate" });
    expect(val.data.elections.length).toEqual(35);
  });
  it("The election in AK-S2 should be for the Senate", async () => {
    var val = await api.getElections({ state: "AK-S2" });
    expect(val.data.elections[0].branch).toEqual("Senate");
  });
  it("There should be 12 elections in VA", async () => {
    var val = await api.getElections({ state: "VA" });
    expect(val.data.elections.length).toEqual(12);
  });
  it("The election in US should be for Presidential", async () => {
    var val = await api.getElections({ state: "US" });
    expect(val.data.elections[0].branch).toEqual("Presidential");
  });
});

// unit tests for Politician API request
describe("API Politician", () => {
  it("all politicians should return 860", async () => {
    var val = await api.getPoliticians();
    expect(val.data.politicians.length).toEqual(899);
  });
  it("Joseph Biden should have his FB", async () => {
    var val = await api.getPoliticians({ name: "Joseph Biden" });
    expect(val.data.politicians[0].facebook).toEqual(
      "https://www.facebook.com/joebiden"
    );
  });
  it("Donald Trump should have his twitter", async () => {
    var val = await api.getPoliticians({ name: "Donald Trump" });
    expect(val.data.politicians[0].twitter).toEqual("realDonaldTrump");
  });
  it("Al Gross district should be AK-S2", async () => {
    var val = await api.getPoliticians({ name: "Al Gross" });
    expect(val.data.politicians[0].district).toEqual("AK-S2");
  });
  it("Al Gross shoud have website", async () => {
    var val = await api.getPoliticians({ name: "Al Gross" });
    expect(val.data.politicians[0].website).toEqual(
      "https://dralgrossak.com/"
    );
  });
  it("There should be 410 democratic politicians", async () => {
    var val = await api.getPoliticians({ party: "DEM" });
    expect(val.data.politicians.length).toEqual(410);
  });
  it("There should be 384 republican politicians", async () => {
    var val = await api.getPoliticians({ party: "REP" });
    expect(val.data.politicians.length).toEqual(384);
  });
  it("There should be 13 republican politicians", async () => {
    var val = await api.getPoliticians({ party: "IND" });
    expect(val.data.politicians.length).toEqual(13);
  });
});

// unit tests for Policies API request
describe("API Policies", () => {
  it("all politicians should return 101", async () => {
    var val = await api.getPolicies();
    expect(val.data.policies.length).toEqual(101);
  });
  it("Clean Energy should have stance url", async () => {
    var val = await api.getPolicies({ name: "Clean Energy" });
    expect(val.data.policies[0].stance).toEqual(
      "https://joebiden.com/clean-energy/"
    );
  });
  it("Work to End Poverty should be state level", async () => {
    var val = await api.getPolicies({ name: "Work to End Poverty" });
    expect(val.data.policies[0].level).toEqual("State");
  });
  it("There should be 54 federal policies", async () => {
    var val = await api.getPolicies({ level: "Federal" });
    expect(val.data.policies.length).toEqual(54);
  });
  it("There should be 47 state policies", async () => {
    var val = await api.getPolicies({ level: "State" });
    expect(val.data.policies.length).toEqual(47);
  });
});
