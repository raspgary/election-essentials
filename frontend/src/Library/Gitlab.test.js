import * as gitlab from "./Gitlab";

const metrics1 = {
  sanjayyepuri: 0,
  "aniket.joshi217": 26,
  deepthi54: 25,
  raspgary: 29,
  Ha3vn: 0,
};

const metrics2 = {
  sanjayyepuri: 3,
  "aniket.joshi217": 20,
  deepthi54: 30,
  raspgary: 29,
  Ha3vn: 5,
};

// unit tests for Gitlab API requests
describe("Gitlab", () => {
  it("test getting unit test count", () => {
    var val = gitlab.getUnitTests(metrics1, console.log);
    expect(val).toEqual(80);
  });
  it("test getting unit test count", () => {
    var val = gitlab.getUnitTests(metrics1, console.log);
    expect(val).toEqual(80);
  });
  it("test getting issues count", () => {
    var val = gitlab.getIssuesCount(metrics2, console.log);
    expect(val).toEqual(87);
  });
  it("test getting issues count", () => {
    var val = gitlab.getIssuesCount(metrics2, console.log);
    expect(val).toEqual(87);
  });
});
