import axios from "axios";

// get all the developers from Gitlab
async function getUsers(commits, setCommits, setCommitsCount) {
  await axios
    .get(
      "https://gitlab.com/api/v4/projects/21353211/repository/contributors"
    )
    .then((res) => {
      updateCommits(res.data, commits, setCommits, setCommitsCount);
    });
}

// get commit info from each developer
function updateCommits(users, commits, setCommits, setCommitsCount) {
  let returnCount = 0;
  for (let i in users) {
    let user = users[i];
    let email = user.email;
    let returnObj = commits;
    returnObj[email] = user.commits;
    returnCount += user.commits;
    setCommits(returnObj);
  }
  setCommitsCount(returnCount);
}

// get issue info for all developers
async function getIssues(issues, setIssues, setIssuesCount) {
  for (var username in issues) {
    await getIssueByUser(username, issues, setIssues);
  }
  getIssuesCount(issues, setIssuesCount);
}

// get issue info from each developer
async function getIssueByUser(username, issues, setIssues) {
  await axios
    .get(
      "https://gitlab.com/api/v4/projects/21353211/issues?assignee_username=" +
        username
    )
    .then((res) => {
      var returnObj = issues;
      returnObj[username] = res.data.length;
      setIssues(returnObj);
    });
}

// get total number of issues
function getIssuesCount(issues, setIssuesCount) {
  var total = 0;
  for (var key in issues) {
    total += issues[key];
  }
  setIssuesCount(total);
  return total;
}

// get total number of unit tests
function getUnitTests(unitTests, setUnitTestsCount) {
  var total = 0;
  for (var key in unitTests) {
    total += unitTests[key];
  }
  setUnitTestsCount(total);
  return total;
}

export { getUnitTests, getIssues, getIssuesCount, getUsers };
