var axios = require("axios");

const hostname = "https://api.thepolitician.me";

// get elections by type or state fields
async function getElections(fields) {
  let params = new URLSearchParams(fields).toString();
  return await axios.get(hostname + "/elections?" + params);
}

// get election by ID in database
async function getElectionById(id) {
  return await axios.get(hostname + "/elections/" + id);
}

// get politicians by name, party, state, and incumbent fields
async function getPoliticians(fields) {
  let params = new URLSearchParams(fields).toString();
  return await axios.get(hostname + "/politicians?" + params);
}

// get politician by ID in database
async function getPoliticianById(id) {
  return await axios.get(hostname + "/politicians/" + id);
}

// get all policies by name or level
async function getPolicies(fields) {
  let params = new URLSearchParams(fields).toString();
  return await axios.get(hostname + "/policies?" + params);
}

// get policy by ID in database
async function getPolicyById(id) {
  return await axios.get(hostname + "/policies/" + id);
}

async function getPolicyWordFrequency() {
  return await axios.get(hostname + "/policies/word_frequency");
}

async function getPoliticianFinances() {
  return await axios.get(hostname + "/politicians/finances");
}

async function getPoliticianByDistrict() {
  return await axios.get(hostname + "/politicians/districts");
}

// Provider Data Endpoints
async function getCountiesText() {
  return await axios.get(hostname + "/provider/counties");
}

async function getDisasters() {
  return await axios.get(hostname + "/provider/disasters");
}

async function getPrograms() {
  return await axios.get(hostname + "/provider/programs");
}

export {
  getElections,
  getPoliticianById,
  getPoliticians,
  getPolicies,
  getElectionById,
  getPolicyById,
  getPolicyWordFrequency,
  getPoliticianFinances,
  getPoliticianByDistrict,
  getCountiesText,
  getDisasters,
  getPrograms
};
