import React from "react";
import algoliasearch from "algoliasearch/lite";
import { connectHits } from "react-instantsearch-dom";
import GridView from "../Components/GridView";
import TableView from "../Components/TableView";

// initialize algolia client
const searchClient = algoliasearch(
  "QZIIN7U5LT",
  "849ec60d9c1d81c2f42a5a3280dea2ba"
);

// wrapper components to convert View Components to Algolia Widgets
const GridHits = connectHits(({ hits, Card }) => (
  <GridView highlight list={hits} Card={Card} />
));

const TableHits = connectHits(({ hits, fields, path }) => (
  <TableView highlight list={hits} fields={fields} path={path} />
));

export { searchClient, GridHits, TableHits };
