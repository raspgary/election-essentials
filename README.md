# CS 373: Software Engineering The Politician Repo

## Member 1

- Name: Gary Wang

- EID: gw7287

- GitLab ID: raspgary

- Estimated completion time: 5

- Actual completion time: 5

## Member 2

- Name: Aniket Joshi

- EID: aj28555

- GitLab ID: aniket.joshi217

- Estimated completion time: 8

- Actual completion time: 10

## Member 3

- Name: Michael Liu

- EID: mml2924

- GitLab ID: Ha3vn

- Estimated completion time: 5

- Actual completion time: 5

## Member 4

- Name: Deepthi Pittala

- EID: dp29968

- GitLab ID: deepthi54

- Estimated completion time: 7

- Actual completion time: 7

## Member 5

- Name: Sanjay Yepuri

- EID: ssy365

- GitLab ID: sanjayyepuri

- Estimated completion time: 15

- Actual completion time: 20

## Project Info

- Project Leader: Aniket Joshi

- Git SHA: 639e19b5533f7d80826c01469a0bce4339f70d2c

- GitLab Pipelines: https://gitlab.com/raspgary/election-essentials/-/pipelines

- Website Link: https://www.thepolitician.me/

- Comments: (any additional comments you have)

- Presentation Video: https://www.youtube.com/watch?v=08yohlNLlUI&ab_channel=GaryWang

