from flask import Flask, request, Response
import json
from application import db
from application.models import Election, Politician, Policy
from application import application
from sqlalchemy.sql.expression import false, true
import requests

# some bits of text for the page.
header_text = "<html>\n<head><title>The Politician API</title> </head>\n<body>"
instructions = (
    "<p>The documentation for our endpoints and RESTful API can be found here:</p>\n"
)
documentation_link = (
    '<p><a href="https://documenter.getpostman.com'
    '/view/12889746/TVRd8B7M">Documentation</a></p>\n'
)
footer_text = "</body>\n</html>"


# add a rule for the index page.
@application.route("/")
def index_route():
    message = header_text + instructions + documentation_link + footer_text
    return Response(response=message, status=200)


### ELECTION ###
@application.route("/elections/<int:election_id>")
def get_election_by_id(election_id):
    x = None
    try:
        query_db = Election.query.filter(Election.id == election_id).all()
        for q in query_db:
            x = q
        db.session.close()
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        db.session.rollback()
        return Response(response=message, status=500)

    if x == None:
        message = "HTTP 404: Election ID " + str(election_id) + " not found"
        return Response(response=message, status=404, mimetype="application/json")

    return Response(
        response=json.dumps(x.jsonify()), status=200, mimetype="application/json"
    )


@application.route("/elections")
def get_elections_by_params():
    state = request.args.get("state")
    branch = request.args.get("type")

    x = []
    query = Election.query
    if state is not None:
        query = query.filter(Election.state.like(str(state[:2]) + "%"))
    if branch is not None:
        query = query.filter(Election.branch == branch)
    query_db = query.all()

    try:
        for q in query_db:
            x.append(q.jsonify())
        db.session.close()
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        db.session.rollback()
        return Response(response=message, status=500)

    obj = {"elections": x}

    return Response(response=json.dumps(obj), status=200, mimetype="application/json")


### POLITICIAN ###
@application.route("/politicians/<int:politician_id>")
def get_politician_by_id(politician_id):

    x = None
    try:
        query_db = Politician.query.filter(Politician.id == politician_id).all()
        for q in query_db:
            x = q
        db.session.close()
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        db.session.rollback()
        return Response(response=message, status=500)

    if x == None:
        message = "HTTP 404: Politician ID " + str(politician_id) + " not found"
        return Response(response=message, status=404, mimetype="application/json")

    return Response(
        response=json.dumps(x.jsonify()), status=200, mimetype="application/json"
    )


def getFunc(bool):
    if bool == False:
        return false()
    return true()


@application.route("/politicians")
def get_politicians_by_params():
    name = request.args.get("name")
    party = request.args.get("party")
    incumbent = request.args.get("incumbent")
    state = request.args.get("state")
    election_id = request.args.get("election_id")

    x = []

    # name endpoint
    if name != None:
        if party != None or incumbent != None or state != None or election_id != None:
            message = "HTTP 400: Can not combine multiple queries with a name search"
            return Response(response=message, status=400, mimetype="application/json")
        try:
            query_db = Politician.query.filter(Politician.name == name).all()
            for q in query_db:
                x.append(q.jsonify())
            db.session.close()
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            db.session.rollback()
            return Response(response=message, status=500)

    # election id endpoint
    elif election_id != None:
        if party != None or incumbent != None or state != None or name != None:
            message = (
                "HTTP 400: Can not combine multiple queries with an election_id search"
            )
            return Response(response=message, status=400, mimetype="application/json")
        try:
            query_db = Politician.query.filter(
                Politician.election_id == election_id
            ).all()
            for q in query_db:
                x.append(q.jsonify())
            db.session.close()
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            db.session.rollback()
            return Response(response=message, status=500)

    # optional params endpoint
    else:
        query = Politician.query
        if party != None:
            query = query.filter(Politician.party == party)
        if incumbent != None:
            query = query.filter(Politician.incumbent == getFunc(incumbent))
        if state != None:
            query = query.filter(Politician.state.like(str(state[:2]) + "%"))
        query_db = query.all()

        try:
            for q in query_db:
                x.append(q.jsonify())
            db.session.close()
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            db.session.rollback()
            return Response(response=message, status=500)

    obj = {"politicians": x}

    return Response(response=json.dumps(obj), status=200, mimetype="application/json")


### POLICY ###
@application.route("/policies/<int:policy_id>")
def get_policies_by_id(policy_id):
    x = None
    try:
        query_db = Policy.query.filter(Policy.id == policy_id).all()
        for q in query_db:
            x = q
        db.session.close()
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        db.session.rollback()
        return Response(response=message, status=500)

    if x == None:
        message = "HTTP 404: Policy ID " + str(policy_id) + " not found"
        return Response(response=message, status=404, mimetype="application/json")

    return Response(
        response=json.dumps(x.jsonify()), status=200, mimetype="application/json"
    )


@application.route("/policies")
def get_policies_by_params():
    name = request.args.get("name")
    level = request.args.get("level")
    politician_id = request.args.get("politician_id")

    if name != None:
        if level != None or politician_id != None:
            message = "HTTP 400: Can not combine multiple queries with a name search"
            return Response(response=message, status=400, mimetype="application/json")
    if level != None:
        if name != None or politician_id != None:
            message = "HTTP 400: Can not combine multiple queries with a level search"
            return Response(response=message, status=400, mimetype="application/json")
    if politician_id != None:
        if level != None or name != None:
            message = (
                "HTTP 400: Can not combine multiple queries with a politician_id search"
            )
            return Response(response=message, status=400, mimetype="application/json")

    x = []
    query = Policy.query
    if name != None:
        query = query.filter(Policy.name == name)
    elif level != None:
        query = query.filter(Policy.level == level)
    elif politician_id != None:
        query = query.filter(Policy.politician_id == politician_id)
    query_db = query.all()

    try:
        for q in query_db:
            x.append(q.jsonify())
        db.session.close()
    except Exception as e:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        db.session.rollback()
        return Response(response=message, status=500)

    obj = {"policies": x}

    return Response(response=json.dumps(obj), status=200, mimetype="application/json")
    

@application.route("/policies/word_frequency")
def get_policies_word_frequency():
    query_db = Policy.query.all()
    stop_words = set()
    
    file1 = open('stopwords.txt', 'r')
    lines = file1.readlines()
    for line in lines:
        stop_words.add(line.strip())
    
    map = {}
    try:
        for q in query_db:
            for word in q.name.split():
                if word not in map:
                    map[word] = 0
                map[word] += 1
            for word in q.description.split():
                if word not in map:
                    map[word] = 0
                map[word] += 1
    except Exception as e:
        return Response(response="error", status=500)
    result = []
    for key in map:
        if key.lower() not in stop_words:
            result.append({ "text": key, "value": map[key] })
    return Response(response=json.dumps(result), status=200, mimetype="application/json")

@application.route("/politicians/finances")
def get_politicians_finances():
    query_db = Politician.query.filter(Politician.contributions > 0).order_by(Politician.contributions.desc())
    l = []
    try:
        for q in query_db:
            obj = {"x": q.name, "y": int(q.contributions)}
            l.append(obj)
    except Exception as e:
        return Response(response="error", status=500)
    
    return Response(response=json.dumps(l), status=200, mimetype="application/json")

@application.route("/politicians/districts")
def get_politicians_districts():
    query_db = Politician.query.all()
    l = {}
    try:
        for q in query_db:
            if q.district not in l:
                l[q.district] = []
            l[q.district].append(q.name)
    except Exception as e:
        return Response(response="error", status=500)
    
    return Response(response=json.dumps(l), status=200, mimetype="application/json")
    
@application.route("/provider/counties")
def get_provider_counties():
    r = requests.get("https://www.disasterinfo.me/api/counties?limit=3008")
    res = json.loads(r.text)
    res = res[:len(res)-1]
    l = []
    map = {}
    for element in res:
        type = element["County"]["name"]
        map[type] = len(element["County"]["disasters"])
    map = dict(sorted(map.items(), key=lambda x: x[1], reverse=True)[:50])
    for key in map:
        l.append({"text": key, "value":map[key]})
    return Response(response=json.dumps(l), status=200, mimetype="application/json")
    
@application.route("/provider/disasters")
def get_provider_disasters():
    r = requests.get("https://www.disasterinfo.me/api/disasters?limit=3642")
    res = json.loads(r.text)
    res = res[:len(res)-1]
    l = []
    map = {}
    sum = 0
    for element in res:
        type = element["Disaster"]["disaster_type"]
        if type not in map:
            map[type] = 0
        map[type] += 1
        sum += 1
    sort = sorted(map.items(), key=lambda x : x[1], reverse=True)
    top10 = dict(sort[0:6])
    remainder = 0.0
    bottom = dict(sort[6:])
    for key in bottom:
        remainder += bottom[key]
    top10["Other"] = remainder
    for key in top10:
        l.append({"x": key, "y":float(map[key])/float(sum)})
    return Response(response=json.dumps(l), status=200, mimetype="application/json")
    
@application.route("/provider/programs")
def get_provider_programs():
    r = requests.get("https://www.disasterinfo.me/api/programs?limit=20217")
    res = json.loads(r.text)
    res = res[:len(res)-1]
            
    l = []
    map = {}
    for element in res:
        type = element["Program"]["name"]
        map[type] = element["Program"]["budget"]
    map = dict(sorted(map.items(), key=lambda x : (0 if x[1] == None else x[1]), reverse=True)[:20])
    for key in map:
        l.append({"x": key, "y":map[key]})
    return Response(response=json.dumps(l), status=200, mimetype="application/json")

# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.\
    application.debug = True
    application.run(host="0.0.0.0")
