import json
import requests
from unittest import main, TestCase
from db_insert import (
    genElections,
    genElectionsMap,
    genPoliticians,
    genPoliticianMap,
    genPolicies,
    genPollsMap,
)


class TestBackend(TestCase):
    local_url = "http://localhost:5000/"
    # root_url = local_url
    root_url = "https://api.thepolitician.me/"

    ### CSV TESTS ###

    def test1(self):
        assert True

    def test_elections_csv_1(self):
        elections, map = genElections("./csv/elections.csv", {})
        assert len(elections) == 471
        assert len(map) == 471
        assert len(map["US"]) == 2
        assert set(map["US"]) == {"Joseph Biden", "Donald Trump"}
        assert elections[0].branch == "Presidential"
        assert elections[1].branch == "Senate"
        assert elections[36].branch == "House"

    def test_elections_csv_2(self):
        elections, map = genElections("./csv/elections.csv", {})
        assert len(elections) == 471
        assert len(map) == 471
        mapping = genElectionsMap(map)
        assert len(mapping) == 1068
        assert mapping["Joseph Biden"] == 1
        assert mapping["Donald Trump"] == 1
        assert mapping["John Cornyn"] == 5

    # add a test for senate_polls.csv

    def test_politicians_csv_1(self):
        elections, map = genElections("./csv/elections.csv", {})
        assert len(elections) == 471
        assert len(map) == 471
        electionsMap = genElectionsMap(map)
        assert len(electionsMap) == 1068
        pollsMap = genPollsMap("./csv/senate_polls2.csv")
        politicians = genPoliticians("./csv/politicians.csv", electionsMap, pollsMap)
        assert len(politicians) == 899
        assert politicians[0].name == "Joseph Biden"
        assert politicians[0].party == "DEM"
        assert politicians[1].name == "Donald Trump"
        assert politicians[1].party == "REP"

    def test_politician_map_1(self):
        politicianMap = genPoliticianMap()
        assert len(politicianMap) == 899
        assert politicianMap["Joseph Biden"] == 1
        assert politicianMap["Donald Trump"] == 2

    def test_politician_map_2(self):
        politicianMap = genPoliticianMap()
        assert len(politicianMap) == 899
        policies = genPolicies("./csv/policies2.csv", politicianMap)
        assert len(policies) == 101

    ### POLITICIANS ###

    # path parameters and all
    def test_politicians_1(self):
        r = requests.get(self.root_url + "politicians/1")
        dict = json.loads(r.text)
        assert dict["id"] == 1
        assert dict["name"] == "Joseph Biden"
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_2(self):
        r = requests.get(self.root_url + "politicians/2")
        dict = json.loads(r.text)
        assert dict["id"] == 2
        assert dict["name"] == "Donald Trump"
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_all(self):
        r = requests.get(self.root_url + "politicians")
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 899
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    # query parameters
    def test_politicians_name(self):
        payload = {"name": "John Cornyn"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 1
        assert dict["politicians"][0]["name"] == "John Cornyn"
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_party(self):
        payload = {"party": "DEM"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 410
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_state(self):
        payload = {"state": "TX"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 78
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_party_state(self):
        payload = {"party": "DEM", "state": "TX"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 32
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_incumbent(self):
        payload = {"incumbent": "true"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 339
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_incumbent_state(self):
        payload = {"incumbent": "true", "state": "TX"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 23
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_incumbent_party(self):
        payload = {"incumbent": "true", "party": "DEM"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 187
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_incumbent_party_state(self):
        payload = {"incumbent": "true", "party": "DEM", "state": "TX"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 11
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_election_id(self):
        payload = {"election_id": "1"}
        r = requests.get(self.root_url + "politicians", payload)
        dict = json.loads(r.text)
        assert len(dict["politicians"]) == 2
        assert dict["politicians"][0]["name"] == "Joseph Biden"
        assert dict["politicians"][1]["name"] == "Donald Trump"
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    ### POLICY ###

    # all and path parameter
    def test_policies_all(self):
        r = requests.get(self.root_url + "policies")
        dict = json.loads(r.text)
        assert len(dict["policies"]) == 101
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_policies_1(self):
        r = requests.get(self.root_url + "policies/1")
        dict = json.loads(r.text)
        assert dict["id"] == 1
        assert dict["name"] == "Clean Energy"
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    # query parameters
    def test_policies_level(self):
        payload = {"level": "Federal"}
        r = requests.get(self.root_url + "policies", payload)
        dict = json.loads(r.text)
        assert len(dict["policies"]) == 54
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_policies_name(self):
        payload = {"name": "Clean Energy"}
        r = requests.get(self.root_url + "policies", payload)
        dict = json.loads(r.text)
        assert len(dict["policies"]) == 1
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_policies_politician_id(self):
        payload = {"politician_id": 1}
        r = requests.get(self.root_url + "policies", payload)
        dict = json.loads(r.text)
        assert len(dict["policies"]) == 4
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    ### ELECTIONS ###

    # all and path parameters
    def test_elections_all(self):
        r = requests.get(self.root_url + "elections")
        dict = json.loads(r.text)
        assert len(dict["elections"]) == 471
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_elections_1(self):
        r = requests.get(self.root_url + "elections/1")
        dict = json.loads(r.text)
        assert dict["branch"] == "Presidential"
        assert dict["id"] == 1
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    # query parameters
    def test_elections_type(self):
        payload = {"type": "Senate"}
        r = requests.get(self.root_url + "elections", payload)
        dict = json.loads(r.text)
        assert len(dict["elections"]) == 35
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_elections_state(self):
        payload = {"state": "TX"}
        r = requests.get(self.root_url + "elections", payload)
        dict = json.loads(r.text)
        assert len(dict["elections"]) == 37
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    def test_elections_type_state(self):
        payload = {"type": "Senate", "state": "TX"}
        r = requests.get(self.root_url + "elections", payload)
        dict = json.loads(r.text)
        assert len(dict["elections"]) == 1
        assert r.status_code == 200
        if "https" in self.root_url:
            assert "https" in r.url

    ### ERRORS ###

    # test incorrect path parameters
    def test_politicians_2000(self):
        r = requests.get(self.root_url + "politicians/2000")
        message = r.text
        assert message == "HTTP 404: Politician ID 2000 not found"
        assert r.status_code == 404
        if "https" in self.root_url:
            assert "https" in r.url

    def test_elections_2000(self):
        r = requests.get(self.root_url + "elections/2000")
        message = r.text
        assert message == "HTTP 404: Election ID 2000 not found"
        assert r.status_code == 404
        if "https" in self.root_url:
            assert "https" in r.url

    def tests_policies_2000(self):
        r = requests.get(self.root_url + "policies/2000")
        message = r.text
        assert message == "HTTP 404: Policy ID 2000 not found"
        assert r.status_code == 404
        if "https" in self.root_url:
            assert "https" in r.url

    # test invalid combinational queries
    def test_politicians_name_and_other(self):
        payload = {"name": "Joseph Biden", "party": "DEM"}
        r = requests.get(self.root_url + "politicians", payload)
        message = r.text
        assert (
            message == "HTTP 400: Can not combine multiple queries with a name search"
        )
        assert r.status_code == 400
        if "https" in self.root_url:
            assert "https" in r.url

    def test_politicians_election_id_and_other(self):
        payload = {"election_id": "1", "incumbent": "true"}
        r = requests.get(self.root_url + "politicians", payload)
        message = r.text
        assert (
            message
            == "HTTP 400: Can not combine multiple queries with an election_id search"
        )
        assert r.status_code == 400
        if "https" in self.root_url:
            assert "https" in r.url

    def test_policies_name_and_other(self):
        payload = {"name": "Agriculture", "level": "Federal"}
        r = requests.get(self.root_url + "policies", payload)
        message = r.text
        assert (
            message == "HTTP 400: Can not combine multiple queries with a name search"
        )
        assert r.status_code == 400
        if "https" in self.root_url:
            assert "https" in r.url

    def test_policies_level_and_other(self):
        payload = {"politician_id": "1", "level": "Federal"}
        r = requests.get(self.root_url + "policies", payload)
        message = r.text
        assert (
            message == "HTTP 400: Can not combine multiple queries with a level search"
        )
        assert r.status_code == 400
        if "https" in self.root_url:
            assert "https" in r.url


if __name__ == "__main__":
    main()
