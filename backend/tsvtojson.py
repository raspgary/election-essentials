import numpy as np
import csv
import json

state_tsv = 'state_tsv.tsv'

with open(state_tsv, "r") as f:
    read_tsv = csv.reader(f, delimiter='\t')
    reader = iter(read_tsv)

    next(reader)

    row_data = {row[0]: { 'state_id': row[1], 'state_name': row[2]} for row in reader}

    print(json.dumps(row_data))


