import csv
import datetime
from application import db
from application.models import Election, Politician, Policy

"""
converts csv data into politician objects
returns a list of politicians to insert into the database
takes in a mapping of name to election_id and name to polls
"""
def genPoliticians(file1, electionsMap, pollsMap):
    result = []
    with open(file1, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            key = (row["party"], row["district"])
            pct = 0.0
            if key in pollsMap:
                pct = pollsMap[key]
            data = Politician(
                name=row["name"],
                party=row["party"],
                state=row["state"],
                district=row["district"],
                election_id=electionsMap[row["name"]],
                experience="",
                contributions=row["total_contributions"],
                photo=row["photo"],
                website=row["url"],
                platform="",
                twitter=row["twitter_user"],
                facebook=row["facebook_url"],
                instagram="",
                incumbent=(row["incumbent"] == "TRUE"),
                address=row["mailing_address"],
                gender=row["gender"],
                poll=pct,
            )
            result.append(data)
    return result
"""
converts csv data into politician objects
inserts politician objects into database
"""
def insertPoliticians(file1, electionsMap, pollsMap):
    politicians = genPoliticians(file1, electionsMap, pollsMap)
    for politician in politicians:
        try:
            db.session.add(politician)
            db.session.commit()
            db.session.close()
        except:
            print("rollback")
            db.session.rollback()
"""
converts csv data into elections objects
returns a list of elections to insert into the database
returns a mapping of district to politician names
"""
def genElections(file1, map):
    result = []
    with open(file1, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            dems = row["democrats"].split(", ")
            reps = row["republicans"].split(", ")
            inde = row["independents"].split(", ")
            list = []
            for dem in dems:
                if dem:
                    list.append(dem)
            for dem in reps:
                if dem:
                    list.append(dem)
            for dem in inde:
                if dem:
                    list.append(dem)

            split = row["district"].split("-")
            district = ""
            if len(split) > 1:
                if split[1][0] == "S":
                    district = int(split[1][1:])
                else:
                    district = int(split[1])
            else:
                district = 0
            data = Election(
                state=row["district"],
                district=district,
                branch=row["branch"],
                url="",
                date=datetime.datetime(2020, 11, 3),
                incumbent_id=0,
                election_url="",
                polls="",
            )
            map[row["district"]] = list
            result.append(data)
    return result, map
"""
converts csv data into election objects
inserts election objects into database
"""
def insertElections(file1, map):
    elections, map = genElections(file1, map)
    for election in elections:
        try:
            db.session.add(election)
            db.session.commit()
            db.session.close()
        except:
            print("rollback")
            db.session.rollback()
    return map
"""
retrieves election data and maps each politician name to an election id
"""
def genElectionsMap(inputmap):
    map = {}
    try:
        query_db = Election.query.all()
        for q in query_db:
            for person in inputmap[q.state]:
                map[person] = q.id
        db.session.close()
    except:
        db.session.rollback()
    return map
"""
retrieves politician data and maps each politician to their id
"""
def genPoliticianMap():
    map = {}
    try:
        query_db = Politician.query.all()
        for q in query_db:
            map[q.name] = q.id
        db.session.close()
    except:
        db.session.rollback()
    return map
"""
converts csv data into policy objects
returns a list of policies to insert into the database
takes in a mapping of name to politician_id
"""
def genPolicies(file1, map):
    result = []
    with open(file1, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            data = Policy(
                name=row["name"],
                politician_id=map[row["politician"]],
                existing="",
                level=row["level"],
                stance=row["stance"],
                news=row["news"],
                social_media="",
                debate_coverage=row["debate"],
                party=row["party"],
                image=row["image"],
                subheader=row["subheader"],
                description=row["description"],
            )
            result.append(data)
    return result
"""
converts csv data into policy objects
stores policy objects into db
"""
def insertPolicies(file1, map):
    policies = genPolicies(file1, map)
    for policy in policies:
        try:
            db.session.add(policy)
            db.session.commit()
            db.session.close()
        except:
            print("rollback")
            db.session.rollback()
"""
retrieves politician data
maps politiican name yo politician id
"""
def genPollMap():
    map = {}
    try:
        query_db = Politician.query.all()
        for q in query_db:
            district = q.district
            if "S" in district:
                district = district[:4]
            map[(q.party, district)] = q.id
        db.session.close()
    except:
        db.session.rollback()
    return map
"""
converts csv data into polling data
returns a map of politician name to polling data
"""
def genPollsMap(file1):
    result = {}
    with open(file1, newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            cycle = row["cycle"]
            candidate = row["candidate_name"]
            party = row["candidate_party"]
            office = row["office_type"].split()[1]
            state = us_state_abbrev[row["state"]]
            if cycle == "2020":
                district = state
                if office == "Senate":
                    district += "-S2"
                elif office == "House":
                    district += "-" + row["seat_number"]
                key = (party, district)
                if key not in result:
                    result[key] = row["pct"]
    return result
"""
default map for converting long form state name
into abbreviation
"""
us_state_abbrev = {
    "Alabama": "AL", "Alaska": "AK",
    "American Samoa": "AS", "Arizona": "AZ",
    "Arkansas": "AR", "California": "CA",
    "Colorado": "CO", "Connecticut": "CT",
    "Delaware": "DE", "District of Columbia": "DC",
    "Florida": "FL", "Georgia": "GA",
    "Guam": "GU", "Hawaii": "HI",
    "Idaho": "ID", "Illinois": "IL",
    "Indiana": "IN", "Iowa": "IA",
    "Kansas": "KS", "Kentucky": "KY",
    "Louisiana": "LA", "Maine": "ME",
    "Maryland": "MD", "Massachusetts": "MA",
    "Michigan": "MI", "Minnesota": "MN",
    "Mississippi": "MS", "Missouri": "MO",
    "Montana": "MT", "Nebraska": "NE",
    "Nevada": "NV", "New Hampshire": "NH",
    "New Jersey": "NJ", "New Mexico": "NM",
    "New York": "NY", "North Carolina": "NC",
    "North Dakota": "ND", "Northern Mariana Islands": "MP",
    "Ohio": "OH", "Oklahoma": "OK",
    "Oregon": "OR", "Pennsylvania": "PA",
    "Puerto Rico": "PR", "Rhode Island": "RI",
    "South Carolina": "SC", "South Dakota": "SD",
    "Tennessee": "TN", "Texas": "TX",
    "Utah": "UT", "Vermont": "VT",
    "Virgin Islands": "VI", "Virginia": "VA",
    "Washington": "WA", "West Virginia": "WV",
    "Wisconsin": "WI", "Wyoming": "WY",
}


if __name__ == "__main__":
    db.create_all()
    print("DB created.")

    map = {}
    map = insertElections("./csv/elections.csv", map)

    electionMap = genElectionsMap(map)
    pollsMap = genPollsMap("./csv/senate_polls2.csv")
    insertPoliticians("./csv/politicians.csv", electionMap, pollsMap)

    politicianMap = genPoliticianMap()
    insertPolicies("./csv/policies2.csv", politicianMap)


"""
SET foreign_key_checks = 0;DROP TABLE IF EXISTS election,politician,policy;SET foreign_key_checks = 1;
"""
