from algoliasearch.search_client import SearchClient
import json
from application import db
from application.models import Election, Politician, Policy

# create global algolia client
client = SearchClient.create("QZIIN7U5LT", "6df7af40f801d852d4d27554a2c212b7")

"""
generate election json data from database
"""


def getElections():
    result = []
    try:
        query_db = Election.query.all()
        for q in query_db:
            map = q.jsonify()
            candidates = []
            for candidate in q.candidates:
                policies = candidate.policies
                cand_json = candidate.jsonify()
                cand_json["policies"] = [policy.jsonify() for policy in policies]
                candidates.append(cand_json)
            map["candidates"] = candidates
            result.append(map)
        db.session.close()
    except:
        db.session.rollback()
    return result


"""
upload election json to algolia
set searchable parameters
"""


def uploadElections():
    index = client.init_index("dev_elections")
    index.clear_objects()
    batch = json.load(open("./csv/elections.json"))
    index.save_objects(batch, {"autoGenerateObjectIDIfNotExist": True})

    index.set_settings(
        {
            "searchableAttributes": [
                "state",
                "district",
                "branch",
                "date",
                "candidates.name",
                "candidates.state",
                "candidates.district",
                "candidates.party",
                "candidates.website",
                "candidates.twitter",
                "candidates.address",
                "candidates.facebook",
                "candidates.policies.level",
                "candidates.policies.stance",
                "candidates.policies.news",
                "candidates.policies.party",
            ]
        }
    )


"""
generate json data for politicians
"""


def getPoliticians():
    result = []
    try:
        query_db = Politician.query.all()
        for q in query_db:
            election = Election.query.filter(Election.id == q.election_id).first()
            map = q.jsonify()
            map["election"] = election.jsonify()
            map["election"]["candidates"] = [
                candidate.jsonify() for candidate in election.candidates
            ]
            map["policies"] = [policy.jsonify() for policy in q.policies]
            result.append(map)
        db.session.close()
    except:
        db.session.rollback()
    return result


"""
upload politician json to algolia
set searchable parameters
"""


def uploadPoliticians():
    index = client.init_index("dev_politicians")
    index.clear_objects()
    batch = json.load(open("./csv/politicians.json"))
    index.save_objects(batch, {"autoGenerateObjectIDIfNotExist": True})

    index.set_settings(
        {
            "searchableAttributes": [
                "name",
                "state",
                "district",
                "party",
                "election.state",
                "election.district",
                "election.branch",
                "election.date",
                "twitter",
                "address",
                "facebook",
                "policies.level",
                "policies.stance",
                "policies.news",
                "policies.party",
                "election.candidates.name",
                "election.candidates.state",
                "election.candidates.district",
                "election.candidates.party",
                "election.candidates.website",
                "election.candidates.twitter",
                "election.candidates.address",
                "election.candidates.facebook",
            ]
        }
    )


"""
generate policy json data from database
"""


def getPolicies():
    result = []
    try:
        query_db = Policy.query.all()
        for q in query_db:
            politician = Politician.query.filter(
                Politician.id == q.politician_id
            ).first()
            map = q.jsonify()
            map["politician"] = politician.jsonify()

            election = Election.query.filter(
                Election.id == politician.election_id
            ).first()
            map["politician"]["election"] = election.jsonify()
            map["politician"]["election"]["candidates"] = [
                candidate.jsonify() for candidate in election.candidates
            ]
            result.append(map)
        db.session.close()
    except:
        db.session.rollback()
    return result


"""
upload policy json to algolia
set searchable parameters
"""


def uploadPolicies():
    index = client.init_index("dev_policies")
    index.clear_objects()
    batch = json.load(open("./csv/policies.json"))
    index.save_objects(batch, {"autoGenerateObjectIDIfNotExist": True})

    index.set_settings(
        {
            "searchableAttributes": [
                "politician.name",
                "politician.state",
                "politician.district",
                "politician.party",
                "politician.election.state",
                "politician.election.district",
                "politician.election.branch",
                "politician.election.date",
                "politician.twitter",
                "politician.address",
                "politician.facebook",
                "level",
                "stance",
                "news",
                "party",
                "politician.election.candidates.name",
                "politician.election.candidates.state",
                "politician.election.candidates.district",
                "politician.election.candidates.party",
                "politician.election.candidates.website",
                "politician.election.candidates.twitter",
                "politician.election.candidates.address",
                "politician.election.candidates.facebook",
            ]
        }
    )


if __name__ == "__main__":
    with open("./csv/elections.json", "w") as outfile:
        json.dump(getElections(), outfile)
    uploadElections()

    with open("./csv/politicians.json", "w") as outfile:
        json.dump(getPoliticians(), outfile)
    uploadPoliticians()

    with open("./csv/policies.json", "w") as outfile:
        json.dump(getPolicies(), outfile)
    uploadPolicies()
