from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

application = Flask(__name__)
CORS(application)
application.config.from_object("config")
db = SQLAlchemy(application)
