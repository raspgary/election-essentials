from application import db

"""
schema for election table
"""


class Election(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.String(10), nullable=False, unique=False)
    district = db.Column(db.Integer)
    branch = db.Column(db.String(15))
    url = db.Column(db.Text)
    date = db.Column(db.DateTime)
    incumbent_id = db.Column(db.Integer)
    election_url = db.Column(db.Text)
    candidates = db.relationship("Politician", backref="election", lazy=True)

    def __init__(
        self, state, district, branch, url, date, incumbent_id, election_url, polls
    ):
        self.state = state
        self.district = district
        self.branch = branch
        self.url = url
        self.date = date
        self.incumbent_id = incumbent_id
        self.election_url = election_url

    def __repr__(self):
        return self.id

    def jsonify(self):
        obj = {
            "id": self.id,
            "state": self.state,
            "district": self.district,
            "branch": self.branch,
            "url": self.url,
            "date": str(self.date),
            "incumbent id": self.incumbent_id,
            "election url": self.election_url,
        }
        return obj


"""
schema for politician table
"""


class Politician(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=False)
    state = db.Column(db.String(10))
    district = db.Column(db.String(10))
    party = db.Column(db.String(50))
    election_id = db.Column(db.Integer, db.ForeignKey("election.id"), nullable=False)
    experience = db.Column(db.Text)
    contributions = db.Column(db.Float)
    photo = db.Column(db.Text)
    website = db.Column(db.Text)
    platform = db.Column(db.Text)
    twitter = db.Column(db.Text)
    facebook = db.Column(db.Text)
    instagram = db.Column(db.Text)
    incumbent = db.Column(db.Boolean)
    policies = db.relationship("Policy", backref="politician", lazy=True)
    address = db.Column(db.Text)
    gender = db.Column(db.String(10))
    poll = db.Column(db.Float)

    def __init__(
        self,
        name,
        state,
        district,
        party,
        election_id,
        experience,
        contributions,
        photo,
        website,
        platform,
        twitter,
        facebook,
        instagram,
        incumbent,
        address,
        gender,
        poll,
    ):
        self.name = name
        self.state = state
        self.district = district
        self.party = party
        self.election_id = election_id
        self.experience = experience
        self.contributions = contributions
        self.photo = photo
        self.website = website
        self.platform = platform
        self.twitter = twitter
        self.facebook = facebook
        self.instagram = instagram
        self.incumbent = incumbent
        self.address = address
        self.gender = gender
        self.poll = poll

    def __repr__(self):
        return str(self.name)

    def jsonify(self):
        obj = {
            "id": self.id,
            "name": self.name,
            "state": self.state,
            "district": self.district,
            "party": self.party,
            "election id": self.election_id,
            "experience": self.experience,
            "contributions": self.contributions,
            "photo": self.photo,
            "website": self.website,
            "platform": self.platform,
            "twitter": self.twitter,
            "facebook": self.facebook,
            "instagram": self.instagram,
            "incumbent": self.incumbent,
            "address": self.address,
            "gender": self.gender,
            "poll": self.poll,
        }

        return obj


"""
schema for policy table
"""


class Policy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=False)
    politician_id = db.Column(
        db.Integer, db.ForeignKey("politician.id"), nullable=False
    )
    existing = db.Column(db.Text)
    level = db.Column(db.String(20))
    stance = db.Column(db.Text)
    news = db.Column(db.Text)
    social_media = db.Column(db.Text)
    debate_coverage = db.Column(db.Text)
    party = db.Column(db.String(25))
    image = db.Column(db.Text)
    subheader = db.Column(db.Text)
    description = db.Column(db.Text)

    def __init__(
        self,
        name,
        politician_id,
        existing,
        level,
        stance,
        news,
        social_media,
        debate_coverage,
        party,
        image,
        subheader,
        description,
    ):
        self.name = name
        self.politician_id = politician_id
        self.existing = existing
        self.level = level
        self.stance = stance
        self.news = news
        self.social_media = social_media
        self.debate_coverage = debate_coverage
        self.party = party
        self.image = image
        self.subheader = subheader
        self.description = description

    def __repr__(self):
        return self.id

    def jsonify(self):
        obj = {
            "id": self.id,
            "name": self.name,
            "politician id": self.politician_id,
            "existing": self.existing,
            "level": self.level,
            "stance": self.stance,
            "news": self.news,
            "social media": self.social_media,
            "debate coverage": self.debate_coverage,
            "party": self.party,
            "image": self.image,
            "subheader": self.subheader,
            "description": self.description,
        }

        return obj
