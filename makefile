start:
	docker-compose up --build

test-backend:
	cd backend;python tests.py

test-frontend:
	cd frontend;npm run test

test: test-backend test-frontend